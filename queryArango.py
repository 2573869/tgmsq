#run on the Virtual Machine with ArangoDB

# this model in order to run the querys in arangoDB and count the execution times
# you need to change the name of  database which you want to execute the query
from pyArango.connection import *
import pandas as pd
import numpy
import time
import requests
# count the time of running the program
startGeneral = time.time()

# database chosen
# need to change the active database ---- data1gb, data2gb, data4gb, data8gb
database = 'data1gb'
# create connection
conn = Connection(arangoURL='http://localhost:8529', username="root", password="******") #verify=True

# connect to database
db = conn[database]

# read csv of query
# where we find the csv of query
path_0 = '/home/activus/data/queryArango1GB.csv'
dfQuery = pd.read_csv(path_0, sep=",")
# where we create the csv of result
path_1 = '/home/activus/data/'
path_1 = path_1 + 'ResultArangoDB' + str(database) + '.csv'
# create new result frame
dfResult = pd.DataFrame(columns=["Query", "result"])

for index, row in dfQuery.iterrows():
    # get the query one by one
    query = row[1]
    print(row[0])
    print(query)
    # for each query we execute 10 times
    # initialization of list and number of runs
    i = 1
    time_list = []
    while i < 11:
        # every times we count the execution time
        time_start = time.time()
        db.AQLQuery(query, rawResults=False)
        time_end = time.time()
        time_run = time_end - time_start
        time_list.append(time_run)
        i = i + 1

    # delete max and min time of list
    time_list.remove(max(time_list))
    time_list.remove(max(time_list))
    time_final = round(numpy.mean(time_list), 5)
    # add in result

    addline = pd.DataFrame(columns=dfResult.columns)
    addline = addline.append({"Query": row[0], "result": time_final}, ignore_index=True)
    addline.to_csv(path_1, index=False, sep=",", mode='a', header=False)

endGeneral = time.time()
print('time cost', endGeneral - startGeneral, 's')

