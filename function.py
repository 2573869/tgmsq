# this module used for delete the data of instance and change the date of instance
# when we need to modify the data in order to have evolutions

import pandas as pd


# this function allows to delete the information of 1 instance
# df : dataframe which we use
# table_id : id of an instance of a dataframe
# lineName : column which we want to delete
# sk : id of instance we want to delete
def delete_info_for1(df, table_id, lineName, sk):
    df[lineName][df[table_id] == sk] = None


# this function allows to delete the information of 2 instances
def delete_info_for2(df, table_id, lineName, sk):
    df[lineName][(df[table_id] == sk) |
                 (df[table_id] == sk + 1)] = None


# this function allows to delete the information of 3 instances
def delete_info_for3(df, table_id, lineName, sk):
    df[lineName][(df[table_id] == sk) |
                 (df[table_id] == sk + 1) |
                 (df[table_id] == sk + 2)] = None


# this function allows to change the dates of instance when last evolution is about instance and this one is instance
# df : dataframe which we used to delete
# table_id : id of an instance of df
# newSk : sk of this data
def changeDateInstanceInstance(df, table_id, newSk):
    date = pd.to_datetime(df['start_valid_time'][df[table_id] == newSk]).values[0]
    date = date + pd.Timedelta(days=31)
    df['start_valid_time'][df[table_id] == newSk] = date.strftime("%Y-%m-%d")
    date = date + pd.Timedelta(days=30)
    df['end_valid_time'][df[table_id] == newSk] = date.strftime("%Y-%m-%d")


# this function allowed to change the dates of instance when last evolution is about instance and this one is Schema
def changeDateInstanceSchema(df, table_id, newSk):
    date = pd.to_datetime(df['start_valid_time'][df[table_id] == newSk]).values[0]
    date = date + pd.Timedelta(days=31)
    df['start_valid_time'][df[table_id] == newSk] = date.strftime("%Y-%m-%d")
    date = date + pd.Timedelta(days=365)
    df['end_valid_time'][df[table_id] == newSk] = date.strftime("%Y-%m-%d")


# this function allowed to change the dates of instance when last evolution is about Schema and this one is instance
def changeDateSchemaInstance(df, table_id, newSk):
    date = pd.to_datetime(df['start_valid_time'][df[table_id] == newSk]).values[0]
    date = date + pd.Timedelta(days=366)
    df['start_valid_time'][df[table_id] == newSk] = date.strftime("%Y-%m-%d")
    date = date + pd.Timedelta(days=30)
    df['end_valid_time'][df[table_id] == newSk] = date.strftime("%Y-%m-%d")


# this function allowed to change the dates of instance when last evolution is about Schema and this one is Schema
def changeDateSchemaSchema(df, table_id, newSk):
    date = pd.to_datetime(df['start_valid_time'][df[table_id] == newSk]).values[0]
    date = date + pd.Timedelta(days=366)
    df['start_valid_time'][df[table_id] == newSk] = date.strftime("%Y-%m-%d")
    date = date + pd.Timedelta(days=365)
    df['end_valid_time'][df[table_id] == newSk] = date.strftime("%Y-%m-%d")


# this function allows to read the large CSV and avoid error of RAM
def readBigCSV(dir_path):
    reader = pd.read_csv(dir_path, iterator=True, sep="|")
    loop = True
    # read the csv 100K
    chunkSize = 100000
    chunks = []
    while loop:
        try:
            chunk = reader.get_chunk(chunkSize)
            chunks.append(chunk)
        except StopIteration:
            loop = False
    df = pd.concat(chunks, ignore_index=True)
    return df
