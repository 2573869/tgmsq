# this module allows to transfer the .dat files to .csv files and add new data about evolutions on the dimensions: call_center, catalog_page, item, promotion, store, web_page, web_site and has_household_demographics

# This module will be executed automatically when you execute transformDimensionTable.py is executed
import os
import pandas as pd
import function
import numpy as np
import time

# where we find the data .dat
path_0 = '/Users/arslan/Desktop/data1GB/'

# where we create the csv
path_1 = '/Users/arslan/Desktop/data1GB/CSV/'

pd.set_option('mode.chained_assignment', None)
# transfer call_center
dir_path = os.path.join(path_0, "call_center.dat")
dir_path_date = os.path.join(path_0, "date_dim.dat")
new_path = os.path.join(path_1, "call_center.csv")
df = pd.read_table(dir_path, sep="|", header=None)

df.columns = ["call_center_sk:ID(Call_Center)", "call_center_id", "start_valid_time", "end_valid_time",
              "close_date_sk", "date_sk", "name", "class", "employees", "sq_ft",
              "hours", "manager", "mkt_id", "mkt_class", "mkt_desc", "market_manager",
              "division", "division_name", "company", "company_name", "street_number", "street_name",
              "street_type", "suite_number", "city", "county", "state", "zip", "country",
              "gmt_offset", "tax_percentage", ":LABEL"]

df = df.fillna(0)
df['call_center_sk:ID(Call_Center)'] = df['call_center_sk:ID(Call_Center)'].astype(int)
pd.to_datetime(df['start_valid_time'])
pd.to_datetime(df['end_valid_time'])

df = df.drop(columns=["close_date_sk"])
# add label for each row
df[':LABEL'] = 'Call_Center'

dfDate = pd.read_table(dir_path_date, sep="|", header=None)
dfDate = dfDate.iloc[:, [0, 2]]
dfDate.columns = ["date_sk", "date"]
df = pd.merge(df, dfDate, on="date_sk")
df = df.drop(columns=["date_sk"])
df.rename(columns={'date': 'open_date_sk'}, inplace=True)
df_Date = df.open_date_sk
df = df.drop(columns=["open_date_sk"])
df.insert(4, 'open_date_sk', df_Date)

# Count line
nb_df = len(df)
# create a dataframe only with business key
df_evo = df['call_center_id']
# delete repetition key
df_evo = df_evo.unique()
nb_evo = len(df_evo)

for i in df_evo:
    nb = 0
    for j in df['call_center_id']:
        if i == j:
            nb += 1
    if nb == 2:
        sk = df.loc[df['call_center_id'] == i].index.tolist()
        first_sk = sk[0] + 1
        # collect and delete information
        last_sk = sk[1] + 1
        information = df[df['call_center_sk:ID(Call_Center)'] == last_sk]

        # delete information for the 2 first instances
        function.delete_info_for2(df, 'call_center_sk:ID(Call_Center)', 'company_name', first_sk)
        function.delete_info_for2(df, 'call_center_sk:ID(Call_Center)', 'class', first_sk)
        function.delete_info_for2(df, 'call_center_sk:ID(Call_Center)', 'employees', first_sk)
        function.delete_info_for2(df, 'call_center_sk:ID(Call_Center)', 'sq_ft', first_sk)
        function.delete_info_for2(df, 'call_center_sk:ID(Call_Center)', 'hours', first_sk)
        function.delete_info_for2(df, 'call_center_sk:ID(Call_Center)', 'manager', first_sk)
        function.delete_info_for2(df, 'call_center_sk:ID(Call_Center)', 'mkt_id', first_sk)
        function.delete_info_for2(df, 'call_center_sk:ID(Call_Center)', 'mkt_class', first_sk)
        function.delete_info_for2(df, 'call_center_sk:ID(Call_Center)', 'mkt_desc', first_sk)
        function.delete_info_for2(df, 'call_center_sk:ID(Call_Center)', 'market_manager', first_sk)
        function.delete_info_for2(df, 'call_center_sk:ID(Call_Center)', 'division', first_sk)
        function.delete_info_for2(df, 'call_center_sk:ID(Call_Center)', 'division_name', first_sk)

        # delete information for the 1st instance
        function.delete_info_for1(df, 'call_center_sk:ID(Call_Center)', 'street_number', first_sk)
        function.delete_info_for1(df, 'call_center_sk:ID(Call_Center)', 'street_name', first_sk)
        function.delete_info_for1(df, 'call_center_sk:ID(Call_Center)', 'suite_number', first_sk)
        function.delete_info_for1(df, 'call_center_sk:ID(Call_Center)', 'city', first_sk)
        function.delete_info_for1(df, 'call_center_sk:ID(Call_Center)', 'county', first_sk)
        function.delete_info_for1(df, 'call_center_sk:ID(Call_Center)', 'state', first_sk)
        function.delete_info_for1(df, 'call_center_sk:ID(Call_Center)', 'zip', first_sk)
        function.delete_info_for1(df, 'call_center_sk:ID(Call_Center)', 'country', first_sk)
        function.delete_info_for1(df, 'call_center_sk:ID(Call_Center)', 'gmt_offset', first_sk)
        function.delete_info_for1(df, 'call_center_sk:ID(Call_Center)', 'tax_percentage', first_sk)
        # 3er evolution
        # evolution schema
        newSk = len(df) + 1
        # add information
        information_new = df[df['call_center_sk:ID(Call_Center)'] == last_sk]
        # change sk
        information_new['call_center_sk:ID(Call_Center)'] = newSk
        # add information
        information_new['hours'] = information['hours']
        # change dates
        date = pd.to_datetime(information['start_valid_time']) + pd.Timedelta(days=365)
        # change date of last tuple
        df.loc[last_sk-1, 'end_valid_time'] = date[last_sk-1].strftime("%Y-%m-%d")
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateSchemaSchema(df, 'call_center_sk:ID(Call_Center)', newSk)

        # 4th schema evolution
        # evolution schema
        # add information 1er instance
        information_new = df[df['call_center_sk:ID(Call_Center)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['call_center_sk:ID(Call_Center)'] = newSk
        # add information
        information_new['class'] = information['class']
        information_new['employees'] = 'NoValue'
        information_new['sq_ft'] = 'NoValue'
        information_new['manager'] = 'NoValue'
        information_new['division'] = 'NoValue'
        information_new['division_name'] = 'NoValue'
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateSchemaInstance(df, 'call_center_sk:ID(Call_Center)', newSk)

        # add information 2nd instance
        # evolution instance
        information_new = df[df['call_center_sk:ID(Call_Center)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['call_center_sk:ID(Call_Center)'] = newSk
        # add information
        information_new['class'] = information['class']
        information_new['employees'] = information['employees']
        information_new['sq_ft'] = 'NoValue'
        information_new['manager'] = 'NoValue'
        information_new['division'] = 'NoValue'
        information_new['division_name'] = 'NoValue'
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateInstanceInstance(df, 'call_center_sk:ID(Call_Center)', newSk)

        # add information 3rd instance
        # evolution instance
        information_new = df[df['call_center_sk:ID(Call_Center)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['call_center_sk:ID(Call_Center)'] = newSk
        # add information
        information_new['class'] = information['class']
        information_new['employees'] = information['employees']
        information_new['sq_ft'] = information['sq_ft']
        information_new['manager'] = 'NoValue'
        information_new['division'] = 'NoValue'
        information_new['division_name'] = 'NoValue'

        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateInstanceInstance(df, 'call_center_sk:ID(Call_Center)', newSk)

        # add information 4er instance
        # evolution instance
        information_new = df[df['call_center_sk:ID(Call_Center)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['call_center_sk:ID(Call_Center)'] = newSk
        # add information
        information_new['class'] = information['class']
        information_new['employees'] = information['employees']
        information_new['sq_ft'] = information['sq_ft']
        information_new['manager'] = information['manager']
        information_new['division'] = 'NoValue'
        information_new['division_name'] = 'NoValue'

        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateInstanceInstance(df, 'call_center_sk:ID(Call_Center)', newSk)

        # add information 5er instance
        # evolution instance
        information_new = df[df['call_center_sk:ID(Call_Center)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['call_center_sk:ID(Call_Center)'] = newSk
        # add information
        information_new['class'] = information['class']
        information_new['employees'] = information['employees']
        information_new['sq_ft'] = information['sq_ft']
        information_new['manager'] = information['manager']
        information_new['division'] = information['division']
        information_new['division_name'] = information['division_name']
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateInstanceInstance(df, 'call_center_sk:ID(Call_Center)', newSk)

        # 5er evolution
        # add information for 1er schema
        information_new = df[df['call_center_sk:ID(Call_Center)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['call_center_sk:ID(Call_Center)'] = newSk
        # add information
        information_new['mkt_id'] = information['mkt_id']
        information_new['mkt_class'] = information['mkt_class']
        information_new['mkt_desc'] = 'NoValue'
        information_new['market_manager'] = 'NoValue'
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateInstanceSchema(df, 'call_center_sk:ID(Call_Center)', newSk)

        # add information for 2er instance
        # evolution instance
        information_new = df[df['call_center_sk:ID(Call_Center)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['call_center_sk:ID(Call_Center)'] = newSk
        # add information
        information_new['mkt_id'] = information['mkt_id']
        information_new['mkt_class'] = information['mkt_class']
        information_new['mkt_desc'] = information['mkt_desc']
        information_new['market_manager'] = 'NoValue'
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateSchemaInstance(df, 'call_center_sk:ID(Call_Center)', newSk)

        # add information for 3er instance
        # evolution instance
        information_new = df[df['call_center_sk:ID(Call_Center)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['call_center_sk:ID(Call_Center)'] = newSk
        # add information
        information_new['mkt_id'] = information['mkt_id']
        information_new['mkt_class'] = information['mkt_class']
        information_new['mkt_desc'] = information['mkt_desc']
        information_new['market_manager'] = information['market_manager']
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateInstanceInstance(df, 'call_center_sk:ID(Call_Center)', newSk)

        # 6er evolution
        # evolution schema
        # add information for 1er instance
        information_new = df[df['call_center_sk:ID(Call_Center)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['call_center_sk:ID(Call_Center)'] = newSk
        # add information
        information_new['tax_percentage'] = 'NoValue'
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateInstanceSchema(df, 'call_center_sk:ID(Call_Center)', newSk)

        # add information for 2er instance
        # evolution instance
        information_new = df[df['call_center_sk:ID(Call_Center)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['call_center_sk:ID(Call_Center)'] = newSk
        # add information
        information_new['tax_percentage'] = information['tax_percentage']
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateSchemaInstance(df, 'call_center_sk:ID(Call_Center)', newSk)
    elif nb == 3:
        sk = df.loc[df['call_center_id'] == i].index.tolist()
        first_sk = sk[0] + 1
        # collect and delete information
        last_sk = sk[2] + 1
        information = df[df['call_center_sk:ID(Call_Center)'] == last_sk]
        # delete information
        function.delete_info_for3(df, 'call_center_sk:ID(Call_Center)',  'company_name', first_sk)
        function.delete_info_for3(df, 'call_center_sk:ID(Call_Center)', 'class', first_sk)
        function.delete_info_for3(df, 'call_center_sk:ID(Call_Center)', 'employees', first_sk)
        function.delete_info_for3(df, 'call_center_sk:ID(Call_Center)', 'sq_ft', first_sk)
        function.delete_info_for3(df, 'call_center_sk:ID(Call_Center)', 'hours', first_sk)
        function.delete_info_for3(df, 'call_center_sk:ID(Call_Center)', 'manager', first_sk)
        function.delete_info_for3(df, 'call_center_sk:ID(Call_Center)', 'mkt_id', first_sk)
        function.delete_info_for3(df, 'call_center_sk:ID(Call_Center)', 'mkt_class', first_sk)
        function.delete_info_for3(df, 'call_center_sk:ID(Call_Center)', 'mkt_desc', first_sk)
        function.delete_info_for3(df, 'call_center_sk:ID(Call_Center)', 'market_manager', first_sk)
        function.delete_info_for3(df, 'call_center_sk:ID(Call_Center)', 'division', first_sk)
        function.delete_info_for3(df, 'call_center_sk:ID(Call_Center)', 'division_name', first_sk)
        function.delete_info_for1(df, 'call_center_sk:ID(Call_Center)', 'street_number', first_sk)
        function.delete_info_for1(df, 'call_center_sk:ID(Call_Center)', 'street_name', first_sk)
        function.delete_info_for1(df, 'call_center_sk:ID(Call_Center)', 'suite_number', first_sk)
        function.delete_info_for1(df, 'call_center_sk:ID(Call_Center)', 'city', first_sk)
        function.delete_info_for1(df, 'call_center_sk:ID(Call_Center)', 'county', first_sk)
        function.delete_info_for1(df, 'call_center_sk:ID(Call_Center)', 'state', first_sk)
        function.delete_info_for1(df, 'call_center_sk:ID(Call_Center)', 'zip', first_sk)
        function.delete_info_for1(df, 'call_center_sk:ID(Call_Center)', 'country', first_sk)
        function.delete_info_for1(df, 'call_center_sk:ID(Call_Center)', 'gmt_offset', first_sk)
        function.delete_info_for1(df, 'call_center_sk:ID(Call_Center)', 'tax_percentage', first_sk)

        # add information for 3er evolution
        df['hours'][df['call_center_sk:ID(Call_Center)'] == last_sk] = information['hours']
        date = pd.to_datetime(information['start_valid_time']) + pd.Timedelta(days=365)
        df.loc[last_sk - 1, 'end_valid_time'] = date[last_sk - 1].strftime("%Y-%m-%d")

        # 4er evolution
        # evolution schema
        # add information 1er instance
        information_new = df[df['call_center_sk:ID(Call_Center)'] == last_sk]
        # change sk
        newSk = len(df) + 1
        information_new['call_center_sk:ID(Call_Center)'] = newSk
        # add information
        information_new['class'] = information['class']
        information_new['employees'] = 'NoValue'
        information_new['sq_ft'] = 'NoValue'
        information_new['manager'] = 'NoValue'
        information_new['division'] = 'NoValue'
        information_new['division_name'] = 'NoValue'
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateSchemaInstance(df, 'call_center_sk:ID(Call_Center)', newSk)

        # add information 2er instance
        # evolution instance
        information_new = df[df['call_center_sk:ID(Call_Center)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['call_center_sk:ID(Call_Center)'] = newSk
        # add information
        information_new['class'] = information['class']
        information_new['employees'] = information['employees']
        information_new['sq_ft'] = 'NoValue'
        information_new['manager'] = 'NoValue'
        information_new['division'] = 'NoValue'
        information_new['division_name'] = 'NoValue'
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateInstanceInstance(df, 'call_center_sk:ID(Call_Center)', newSk)

        # add information 3er instance
        # evolution instance
        information_new = df[df['call_center_sk:ID(Call_Center)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['call_center_sk:ID(Call_Center)'] = newSk
        # add information
        information_new['class'] = information['class']
        information_new['employees'] = information['employees']
        information_new['sq_ft'] = information['sq_ft']
        information_new['manager'] = 'NoValue'
        information_new['division'] = 'NoValue'
        information_new['division_name'] = 'NoValue'
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateInstanceInstance(df, 'call_center_sk:ID(Call_Center)', newSk)

        # add information 4er instance
        # evolution instance
        information_new = df[df['call_center_sk:ID(Call_Center)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['call_center_sk:ID(Call_Center)'] = newSk
        # add information
        information_new['class'] = information['class']
        information_new['employees'] = information['employees']
        information_new['sq_ft'] = information['sq_ft']
        information_new['manager'] = information['manager']
        information_new['division'] = 'NoValue'
        information_new['division_name'] = 'NoValue'

        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateInstanceInstance(df, 'call_center_sk:ID(Call_Center)', newSk)

        # add information 5er instance
        # evolution instance
        information_new = df[df['call_center_sk:ID(Call_Center)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['call_center_sk:ID(Call_Center)'] = newSk
        # add information
        information_new['class'] = information['class']
        information_new['employees'] = information['employees']
        information_new['sq_ft'] = information['sq_ft']
        information_new['manager'] = information['manager']
        information_new['division'] = information['division']
        information_new['division_name'] = information['division_name']
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateInstanceInstance(df, 'call_center_sk:ID(Call_Center)', newSk)

        # 5er evolution
        # add information for 1er instance
        # evolution schema
        information_new = df[df['call_center_sk:ID(Call_Center)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['call_center_sk:ID(Call_Center)'] = newSk
        # add information
        information_new['mkt_id'] = information['mkt_id']
        information_new['mkt_class'] = information['mkt_class']
        information_new['mkt_desc'] = 'NoValue'
        information_new['market_manager'] = 'NoValue'
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateInstanceSchema(df, 'call_center_sk:ID(Call_Center)', newSk)

        # add information for 2er instance
        # evolution instance
        information_new = df[df['call_center_sk:ID(Call_Center)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['call_center_sk:ID(Call_Center)'] = newSk
        # add information
        information_new['mkt_id'] = information['mkt_id']
        information_new['mkt_class'] = information['mkt_class']
        information_new['mkt_desc'] = information['mkt_desc']
        information_new['market_manager'] = 'NoValue'
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateSchemaInstance(df, 'call_center_sk:ID(Call_Center)', newSk)

        # add information for 3er instance
        # evolution instance
        information_new = df[df['call_center_sk:ID(Call_Center)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['call_center_sk:ID(Call_Center)'] = newSk
        # add information
        information_new['mkt_id'] = information['mkt_id']
        information_new['mkt_class'] = information['mkt_class']
        information_new['mkt_desc'] = information['mkt_desc']
        information_new['market_manager'] = information['market_manager']
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateInstanceInstance(df, 'call_center_sk:ID(Call_Center)', newSk)

        # 6er evolution
        # add information for 1er instance
        # evolution schema
        information_new = df[df['call_center_sk:ID(Call_Center)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['call_center_sk:ID(Call_Center)'] = newSk
        # add information
        information_new['tax_percentage'] = 'NoValue'
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateInstanceSchema(df, 'call_center_sk:ID(Call_Center)', newSk)

        # add information for 2er instance
        # evolution instance
        information_new = df[df['call_center_sk:ID(Call_Center)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['call_center_sk:ID(Call_Center)'] = newSk
        # add information
        information_new['tax_percentage'] = information['tax_percentage']
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateSchemaInstance(df, 'call_center_sk:ID(Call_Center)', newSk)

new_path = os.path.join(path_1, "call_center.csv")
df.to_csv(new_path, index=False, sep=",")


# transfer store
dir_path = os.path.join(path_0, "store.dat")
df = pd.read_table(dir_path, sep="|", header=None)

df = df.iloc[:, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 13, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28]]
df.columns = ["store_sk:ID(Store)", "store_id", "start_valid_time", "end_valid_time", "date_sk", "store_name",
              "number_employees", "floor_space", "hours", "manager", "market_id", "market_desc", "market_manager",
              "street_number", "street_name", "street_type", "suite_number", "city", "county", "state", "zip",
              "country", "gmt_offset", "tax_percentage"]

pd.to_datetime(df['start_valid_time'])
pd.to_datetime(df['end_valid_time'])

df[':LABEL'] = 'Store'

# Count line
nb_df = len(df)
# create a dataframe only with business key
df_evo = df['store_id']
# delete repetition key
df_evo = df_evo.unique()
nb_evo = len(df_evo)

for i in df_evo:
    nb = 0
    for j in df['store_id']:
        if i == j:
            nb += 1
    if nb == 2:
        sk = df.loc[df['store_id'] == i].index.tolist()
        first_sk = sk[0] + 1
        # collect and delete information
        last_sk = sk[1] + 1
        information = df[df['store_sk:ID(Store)'] == last_sk]
        # delete information
        function.delete_info_for2(df, 'store_sk:ID(Store)', 'number_employees', first_sk)
        function.delete_info_for2(df, 'store_sk:ID(Store)', 'floor_space', first_sk)
        function.delete_info_for2(df, 'store_sk:ID(Store)', 'hours', first_sk)
        function.delete_info_for2(df, 'store_sk:ID(Store)', 'manager', first_sk)
        function.delete_info_for2(df, 'store_sk:ID(Store)', 'market_id', first_sk)
        function.delete_info_for2(df, 'store_sk:ID(Store)', 'hours', first_sk)
        function.delete_info_for2(df, 'store_sk:ID(Store)', 'market_desc', first_sk)
        function.delete_info_for2(df, 'store_sk:ID(Store)', 'market_manager', first_sk)
        function.delete_info_for2(df, 'store_sk:ID(Store)', 'tax_percentage', first_sk)
        function.delete_info_for1(df, 'store_sk:ID(Store)', 'street_number', first_sk)
        function.delete_info_for1(df, 'store_sk:ID(Store)', 'street_name', first_sk)
        function.delete_info_for1(df, 'store_sk:ID(Store)', 'street_type', first_sk)
        function.delete_info_for1(df, 'store_sk:ID(Store)', 'suite_number', first_sk)
        function.delete_info_for1(df, 'store_sk:ID(Store)', 'city', first_sk)
        function.delete_info_for1(df, 'store_sk:ID(Store)', 'county', first_sk)
        function.delete_info_for1(df, 'store_sk:ID(Store)', 'state', first_sk)
        function.delete_info_for1(df, 'store_sk:ID(Store)', 'zip', first_sk)
        function.delete_info_for1(df, 'store_sk:ID(Store)', 'country', first_sk)
        function.delete_info_for1(df, 'store_sk:ID(Store)', 'gmt_offset', first_sk)
        function.delete_info_for1(df, 'store_sk:ID(Store)', 'tax_percentage', first_sk)
        # 3er evolution
        # evolution schema
        newSk = len(df) + 1
        # add information
        information_new = df[df['store_sk:ID(Store)'] == last_sk]
        # change sk
        information_new['store_sk:ID(Store)'] = newSk
        # add information
        information_new['number_employees'] = information['number_employees']
        information_new['floor_space'] = information['floor_space']
        information_new['hours'] = information['hours']
        # change dates
        date = pd.to_datetime(information['start_valid_time']) + pd.Timedelta(days=365)
        # change date of last tuple
        df.loc[last_sk-1, 'end_valid_time'] = date[last_sk-1].strftime("%Y-%m-%d")
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateSchemaSchema(df, 'store_sk:ID(Store)', newSk)

        # 4er evolution
        # evolution schema
        # add information for 1er instance
        information_new = df[df['store_sk:ID(Store)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['store_sk:ID(Store)'] = newSk
        # add information
        information_new['manager'] = 'NoValue'
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateSchemaInstance(df, 'store_sk:ID(Store)', newSk)

        # add information for 2er instance
        # evolution instance
        information_new = df[df['store_sk:ID(Store)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['store_sk:ID(Store)'] = newSk
        # add information
        information_new['manager'] = information['manager']
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateInstanceInstance(df, 'store_sk:ID(Store)', newSk)

        # 5er evolution
        # add information for 1er instance
        # evolution schema
        information_new = df[df['store_sk:ID(Store)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['store_sk:ID(Store)'] = newSk
        # add information
        information_new['market_id'] = information['market_id']
        information_new['market_desc'] = 'NoValue'
        information_new['market_manager'] = 'NoValue'
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateInstanceSchema(df, 'store_sk:ID(Store)',  newSk)

        # add information for 2er instance
        # evolution instance
        information_new = df[df['store_sk:ID(Store)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['store_sk:ID(Store)'] = newSk
        # add information
        information_new['market_id'] = information['market_id']
        information_new['market_desc'] = information['market_desc']
        information_new['market_manager'] = 'NoValue'
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateSchemaInstance(df, 'store_sk:ID(Store)', newSk)

        # add information for 3er instance
        # evolution instance
        information_new = df[df['store_sk:ID(Store)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['store_sk:ID(Store)'] = newSk
        # add information
        information_new['market_id'] = information['market_id']
        information_new['market_desc'] = information['market_desc']
        information_new['market_manager'] = information['market_manager']
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateInstanceInstance(df, 'store_sk:ID(Store)', newSk)

        # 6er evolution
        # add information for 1er instance
        # evolution schema
        information_new = df[df['store_sk:ID(Store)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['store_sk:ID(Store)'] = newSk
        # add information
        information_new['tax_percentage'] = 'NoValue'
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateInstanceSchema(df, 'store_sk:ID(Store)', newSk)

        # add information for 2er instance
        # evolution instance
        information_new = df[df['store_sk:ID(Store)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['store_sk:ID(Store)'] = newSk
        # add information
        information_new['tax_percentage'] = information['tax_percentage']
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateSchemaInstance(df, 'store_sk:ID(Store)', newSk)
    elif nb == 3:
        sk = df.loc[df['store_id'] == i].index.tolist()
        first_sk = sk[0] + 1
        # collect and delete information
        last_sk = sk[2] + 1
        information = df[df['store_sk:ID(Store)'] == last_sk]
        # delete information
        function.delete_info_for3(df, 'store_sk:ID(Store)', 'number_employees', first_sk)
        function.delete_info_for3(df, 'store_sk:ID(Store)', 'floor_space', first_sk)
        function.delete_info_for3(df, 'store_sk:ID(Store)', 'hours', first_sk)
        function.delete_info_for3(df, 'store_sk:ID(Store)', 'manager', first_sk)
        function.delete_info_for3(df, 'store_sk:ID(Store)', 'market_id', first_sk)
        function.delete_info_for3(df, 'store_sk:ID(Store)', 'hours', first_sk)
        function.delete_info_for3(df, 'store_sk:ID(Store)', 'market_desc', first_sk)
        function.delete_info_for3(df, 'store_sk:ID(Store)', 'market_manager', first_sk)
        function.delete_info_for3(df, 'store_sk:ID(Store)', 'tax_percentage', first_sk)
        function.delete_info_for1(df, 'store_sk:ID(Store)', 'street_number', first_sk)
        function.delete_info_for1(df, 'store_sk:ID(Store)', 'street_name', first_sk)
        function.delete_info_for1(df, 'store_sk:ID(Store)', 'street_type', first_sk)
        function.delete_info_for1(df, 'store_sk:ID(Store)', 'suite_number', first_sk)
        function.delete_info_for1(df, 'store_sk:ID(Store)', 'city', first_sk)
        function.delete_info_for1(df, 'store_sk:ID(Store)', 'county', first_sk)
        function.delete_info_for1(df, 'store_sk:ID(Store)', 'state', first_sk)
        function.delete_info_for1(df, 'store_sk:ID(Store)', 'zip', first_sk)
        function.delete_info_for1(df, 'store_sk:ID(Store)', 'country', first_sk)
        function.delete_info_for1(df, 'store_sk:ID(Store)', 'gmt_offset', first_sk)
        function.delete_info_for1(df, 'store_sk:ID(Store)', 'tax_percentage', first_sk)

        # add information for 3er evolution
        df['number_employees'][df['store_sk:ID(Store)'] == last_sk] = information['number_employees']
        df['floor_space'][df['store_sk:ID(Store)'] == last_sk] = information['floor_space']
        df['hours'][df['store_sk:ID(Store)'] == last_sk] = information['hours']
        date = pd.to_datetime(information['start_valid_time']) + pd.Timedelta(days=365)
        df.loc[last_sk - 1, 'end_valid_time'] = date[last_sk - 1].strftime("%Y-%m-%d")

        # 4er evolution
        # evolution schema
        # add information for 1er instance
        information_new = df[df['store_sk:ID(Store)'] == last_sk]
        # change sk
        newSk = len(df) + 1
        information_new['store_sk:ID(Store)'] = newSk
        # add information
        information_new['manager'] = 'NoValue'
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateSchemaSchema(df, 'store_sk:ID(Store)', newSk)

        # add information for 2er instance
        # evolution instance
        information_new = df[df['store_sk:ID(Store)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['store_sk:ID(Store)'] = newSk
        # add information
        information_new['manager'] = information['manager']
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateSchemaInstance(df, 'store_sk:ID(Store)', newSk)

        # 5er evolution
        # evolution schema
        # add information for 1er instance
        information_new = df[df['store_sk:ID(Store)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['store_sk:ID(Store)'] = newSk
        # add information
        information_new['market_id'] = information['market_id']
        information_new['market_desc'] = 'NoValue'
        information_new['market_manager'] = 'NoValue'
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateInstanceSchema(df, 'store_sk:ID(Store)', newSk)

        # add information for 2er instance
        # evolution instance
        information_new = df[df['store_sk:ID(Store)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['store_sk:ID(Store)'] = newSk
        # add information
        information_new['market_id'] = information['market_id']
        information_new['market_desc'] = information['market_desc']
        information_new['market_manager'] = 'NoValue'
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateSchemaInstance(df, 'store_sk:ID(Store)', newSk)

        # add information for 3er instance
        # evolution instance
        information_new = df[df['store_sk:ID(Store)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['store_sk:ID(Store)'] = newSk
        # add information
        information_new['market_id'] = information['market_id']
        information_new['market_desc'] = information['market_desc']
        information_new['market_manager'] = information['market_manager']
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateInstanceInstance(df, 'store_sk:ID(Store)', newSk)

        # 6er evolution
        # evolution schema
        # add information for 1er instance
        information_new = df[df['store_sk:ID(Store)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['store_sk:ID(Store)'] = newSk
        # add information
        information_new['tax_percentage'] = 'NoValue'
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateInstanceSchema(df, 'store_sk:ID(Store)', newSk)

        # add information for 2er instance
        # evolution instance
        information_new = df[df['store_sk:ID(Store)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['store_sk:ID(Store)'] = newSk
        # add information
        information_new['tax_percentage'] = information['tax_percentage']
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateSchemaInstance(df, 'store_sk:ID(Store)', newSk)
new_path = os.path.join(path_1, "store.csv")
df.to_csv(new_path, index=False, sep=",")


# transfer web_page
dir_path = os.path.join(path_0, "web_page.dat")
df = pd.read_table(dir_path, sep="|", header=None)

df.columns = ["web_page_sk:ID(Web_Page)", "web_page_id", "start_valid_time", "end_valid_time",
              "creation_date_sk", "access_date_sk", "autogen_flag", "customer_sk", "url", "type",
              "char_count", "link_count", "image_count", "max_ad_count", ":LABEL"]

pd.to_datetime(df['start_valid_time'])
pd.to_datetime(df['end_valid_time'])

df[':LABEL'] = 'Web_Page'

# Count line
nb_df = len(df)
# create a dataframe only with business key
df_evo = df['web_page_id']
# delete repetition key
df_evo = df_evo.unique()
nb_evo = len(df_evo)

for i in df_evo:
    nb = 0
    for j in df['web_page_id']:
        if i == j:
            nb += 1
    if nb == 2:
        sk = df.loc[df['web_page_id'] == i].index.tolist()
        first_sk = sk[0] + 1
        # collect and delete information
        last_sk = sk[1] + 1
        information = df[df['web_page_sk:ID(Web_Page)'] == last_sk]
        date = pd.to_datetime(information['start_valid_time']) + pd.Timedelta(days=365)
        # delete information
        function.delete_info_for2(df, 'web_page_sk:ID(Web_Page)', 'autogen_flag', first_sk)
        function.delete_info_for2(df, 'web_page_sk:ID(Web_Page)', 'customer_sk', first_sk)
        function.delete_info_for2(df, 'web_page_sk:ID(Web_Page)', 'type', first_sk)
        function.delete_info_for2(df, 'web_page_sk:ID(Web_Page)', 'char_count', first_sk)
        function.delete_info_for2(df, 'web_page_sk:ID(Web_Page)', 'link_count', first_sk)
        function.delete_info_for2(df, 'web_page_sk:ID(Web_Page)', 'image_count', first_sk)
        function.delete_info_for2(df, 'web_page_sk:ID(Web_Page)', 'max_ad_count', first_sk)
        function.delete_info_for1(df, 'web_page_sk:ID(Web_Page)', 'access_date_sk', first_sk)
        # evolution for 3er instance
        newSk = len(df) + 1
        # add information
        information_new = df[df['web_page_sk:ID(Web_Page)'] == last_sk]
        # change sk
        information_new['web_page_sk:ID(Web_Page)'] = newSk
        # add information
        information_new['autogen_flag'] = information['autogen_flag']
        # change dates
        df.loc[last_sk-1, 'end_valid_time'] = date[last_sk-1].strftime("%Y-%m-%d")
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateSchemaSchema(df, 'web_page_sk:ID(Web_Page)', newSk)

        # 4er evolution
        # evolution schema
        # add information for 1er instance
        information_new = df[df['web_page_sk:ID(Web_Page)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['web_page_sk:ID(Web_Page)'] = newSk
        # add information
        information_new['char_count'] = information['char_count']
        information_new['link_count'] = 'NoValue'
        information_new['image_count'] = 'NoValue'
        information_new['max_ad_count'] = 'NoValue'
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateSchemaSchema(df, 'web_page_sk:ID(Web_Page)', newSk)

        # add information for 2er instance
        # evolution instance
        information_new = df[df['web_page_sk:ID(Web_Page)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['web_page_sk:ID(Web_Page)'] = newSk
        # add information
        information_new['char_count'] = information['char_count']
        information_new['link_count'] = information['link_count']
        information_new['image_count'] = 'NoValue'
        information_new['max_ad_count'] = 'NoValue'
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateSchemaInstance(df, 'web_page_sk:ID(Web_Page)', newSk)

        # add information for 3er instance
        # evolution instance
        information_new = df[df['web_page_sk:ID(Web_Page)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['web_page_sk:ID(Web_Page)'] = newSk
        # add information
        information_new['char_count'] = information['char_count']
        information_new['link_count'] = information['link_count']
        information_new['image_count'] = information['image_count']
        information_new['max_ad_count'] = 'NoValue'
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateInstanceInstance(df, 'web_page_sk:ID(Web_Page)', newSk)

        # add information for 4er instance
        # evolution instance
        information_new = df[df['web_page_sk:ID(Web_Page)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['web_page_sk:ID(Web_Page)'] = newSk
        # add information
        information_new['char_count'] = information['char_count']
        information_new['link_count'] = information['link_count']
        information_new['image_count'] = information['image_count']
        information_new['max_ad_count'] = information['max_ad_count']
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateInstanceInstance(df, 'web_page_sk:ID(Web_Page)', newSk)
    elif nb == 3:
        sk = df.loc[df['web_page_id'] == i].index.tolist()
        first_sk = sk[0] + 1
        # collect and delete information
        last_sk = sk[2] + 1
        information = df[df['web_page_sk:ID(Web_Page)'] == last_sk]
        # delete information
        function.delete_info_for3(df, 'web_page_sk:ID(Web_Page)', 'autogen_flag', first_sk)
        function.delete_info_for3(df, 'web_page_sk:ID(Web_Page)', 'customer_sk', first_sk)
        function.delete_info_for3(df, 'web_page_sk:ID(Web_Page)', 'type', first_sk)
        function.delete_info_for3(df, 'web_page_sk:ID(Web_Page)', 'char_count', first_sk)
        function.delete_info_for3(df, 'web_page_sk:ID(Web_Page)', 'link_count', first_sk)
        function.delete_info_for3(df, 'web_page_sk:ID(Web_Page)', 'image_count', first_sk)
        function.delete_info_for3(df, 'web_page_sk:ID(Web_Page)', 'max_ad_count', first_sk)
        function.delete_info_for3(df, 'web_page_sk:ID(Web_Page)', 'access_date_sk', first_sk)

        # add information for 3er evolution
        df['autogen_flag'][df['web_page_sk:ID(Web_Page)'] == last_sk] = information['autogen_flag']
        date = pd.to_datetime(information['start_valid_time']) + pd.Timedelta(days=365)
        df.loc[last_sk - 1, 'end_valid_time'] = date[last_sk - 1].strftime("%Y-%m-%d")

        # 4er evolution
        # evolution instance
        # add information for 1er instance
        information_new = df[df['web_page_sk:ID(Web_Page)'] == last_sk]
        # change sk
        newSk = len(df) + 1
        information_new['web_page_sk:ID(Web_Page)'] = newSk
        # add information
        information_new['char_count'] = information['char_count']
        information_new['link_count'] = 'NoValue'
        information_new['image_count'] = 'NoValue'
        information_new['max_ad_count'] = 'NoValue'
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateSchemaSchema(df, 'web_page_sk:ID(Web_Page)', newSk)

        # add information for 2er instance
        # evolution instance
        information_new = df[df['web_page_sk:ID(Web_Page)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['web_page_sk:ID(Web_Page)'] = newSk
        # add information
        information_new['char_count'] = information['char_count']
        information_new['link_count'] = information['link_count']
        information_new['image_count'] = 'NoValue'
        information_new['max_ad_count'] = 'NoValue'
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateSchemaInstance(df, 'web_page_sk:ID(Web_Page)', newSk)

        # add information for 3er instance
        # evolution instance
        information_new = df[df['web_page_sk:ID(Web_Page)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['web_page_sk:ID(Web_Page)'] = newSk
        # add information
        information_new['char_count'] = information['char_count']
        information_new['link_count'] = information['link_count']
        information_new['image_count'] = information['image_count']
        information_new['max_ad_count'] = 'NoValue'
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateInstanceInstance(df, 'web_page_sk:ID(Web_Page)', newSk)

        # add information for 4er instance
        # evolution instance
        information_new = df[df['web_page_sk:ID(Web_Page)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['web_page_sk:ID(Web_Page)'] = newSk
        # add information
        information_new['char_count'] = information['char_count']
        information_new['link_count'] = information['link_count']
        information_new['image_count'] = information['image_count']
        information_new['max_ad_count'] = information['max_ad_count']
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateInstanceInstance(df, 'web_page_sk:ID(Web_Page)', newSk)
new_path = os.path.join(path_1, "web_page.csv")
df.to_csv(new_path, index=False, sep=",")


# transfer web_site
dir_path = os.path.join(path_0, "web_site.dat")
df = pd.read_table(dir_path, sep="|", header=None)

df.columns = ["web_site_sk:ID(Web_Site)", "web_site_id", "start_valid_time", "end_valid_time",
              "name", "open_date_sk", "close_date_sk", "class", "manager", "mkt_id", "mkt_class",
              "mkt_desc", "market_manager", "company_id", "company_name", "street_number", "street_name",
              "street_type", "suite_number", "city", "county", "state", "zip", "country",
              "gmt_offset", "tax_percentage", ":LABEL"]

df = df.drop(columns=["class"])


pd.to_datetime(df['start_valid_time'])
pd.to_datetime(df['end_valid_time'])
df[':LABEL'] = 'Web_Site'

# Count line
nb_df = len(df)
# create a dataframe only with business key
df_evo = df['web_site_id']
# delete repetition key
df_evo = df_evo.unique()
nb_evo = len(df_evo)

for i in df_evo:
    nb = 0
    for j in df['web_site_id']:
        if i == j:
            nb += 1
    if nb == 2:
        sk = df.loc[df['web_site_id'] == i].index.tolist()
        first_sk = sk[0] + 1
        # collect and delete information
        last_sk = sk[1] + 1
        information = df[df['web_site_sk:ID(Web_Site)'] == last_sk]
        # delete information
        function.delete_info_for2(df, 'web_site_sk:ID(Web_Site)', 'manager', first_sk)
        function.delete_info_for2(df, 'web_site_sk:ID(Web_Site)', 'mkt_id', first_sk)
        function.delete_info_for2(df, 'web_site_sk:ID(Web_Site)', 'mkt_class', first_sk)
        function.delete_info_for2(df, 'web_site_sk:ID(Web_Site)', 'market_manager', first_sk)
        function.delete_info_for2(df, 'web_site_sk:ID(Web_Site)', 'tax_percentage', first_sk)
        function.delete_info_for1(df, 'web_site_sk:ID(Web_Site)', 'street_number', first_sk)
        function.delete_info_for1(df, 'web_site_sk:ID(Web_Site)', 'street_name', first_sk)
        function.delete_info_for1(df, 'web_site_sk:ID(Web_Site)', 'street_type', first_sk)
        function.delete_info_for1(df, 'web_site_sk:ID(Web_Site)', 'suite_number', first_sk)
        function.delete_info_for1(df, 'web_site_sk:ID(Web_Site)', 'city', first_sk)
        function.delete_info_for1(df, 'web_site_sk:ID(Web_Site)', 'county', first_sk)
        function.delete_info_for1(df, 'web_site_sk:ID(Web_Site)', 'state', first_sk)
        function.delete_info_for1(df, 'web_site_sk:ID(Web_Site)', 'zip', first_sk)
        function.delete_info_for1(df, 'web_site_sk:ID(Web_Site)', 'country', first_sk)
        function.delete_info_for1(df, 'web_site_sk:ID(Web_Site)', 'gmt_offset', first_sk)
        # evolution for 3er instance
        # evolution schema
        newSk = len(df) + 1
        # add information
        information_new = df[df['web_site_sk:ID(Web_Site)'] == last_sk]
        # change sk
        information_new['web_site_sk:ID(Web_Site)'] = newSk
        # add information
        information_new['manager'] = information['manager']
        # change dates
        date = pd.to_datetime(information['start_valid_time']) + pd.Timedelta(days=365)
        # change date of last tuple
        df.loc[last_sk-1, 'end_valid_time'] = date[last_sk-1].strftime("%Y-%m-%d")
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateSchemaSchema(df, 'web_site_sk:ID(Web_Site)', newSk)

        # 4er evolution
        # add information for 1er instance
        # evolution schema
        information_new = df[df['web_site_sk:ID(Web_Site)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['web_site_sk:ID(Web_Site)'] = newSk
        # add information
        information_new['mkt_id'] = information['mkt_id']
        information_new['mkt_class'] = 'NoValue'
        information_new['market_manager'] = 'NoValue'
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateSchemaSchema(df, 'web_site_sk:ID(Web_Site)', newSk)

        # add information for 2er instance
        # evolution instance
        information_new = df[df['web_site_sk:ID(Web_Site)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['web_site_sk:ID(Web_Site)'] = newSk
        # add information
        information_new['mkt_id'] = information['mkt_id']
        information_new['mkt_class'] = information['mkt_class']
        information_new['market_manager'] = 'NoValue'
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateSchemaInstance(df, 'web_site_sk:ID(Web_Site)', newSk)

        # add information for 3er instance
        # evolution instance
        information_new = df[df['web_site_sk:ID(Web_Site)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['web_site_sk:ID(Web_Site)'] = newSk
        # add information
        information_new['mkt_id'] = information['mkt_id']
        information_new['mkt_class'] = information['mkt_class']
        information_new['market_manager'] = information['market_manager']
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateInstanceInstance(df, 'web_site_sk:ID(Web_Site)', newSk)

        # 5er evolution
        # evolution schema
        # add information for 1er instance
        information_new = df[df['web_site_sk:ID(Web_Site)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['web_site_sk:ID(Web_Site)'] = newSk
        # add information
        information_new['tax_percentage'] = 'NoValue'
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateInstanceSchema(df, 'web_site_sk:ID(Web_Site)', newSk)

        # add information for 2er instance
        information_new = df[df['web_site_sk:ID(Web_Site)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['web_site_sk:ID(Web_Site)'] = newSk
        # add information
        information_new['tax_percentage'] = information['tax_percentage']
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateSchemaInstance(df, 'web_site_sk:ID(Web_Site)', last_sk, newSk)
    elif nb == 3:
        sk = df.loc[df['web_site_id'] == i].index.tolist()
        first_sk = sk[0] + 1
        # collect and delete information
        last_sk = sk[2] + 1
        information = df[df['web_site_sk:ID(Web_Site)'] == last_sk]
        # delete information
        function.delete_info_for3(df, 'web_site_sk:ID(Web_Site)', 'manager', first_sk)
        function.delete_info_for3(df, 'web_site_sk:ID(Web_Site)', 'mkt_id', first_sk)
        function.delete_info_for3(df, 'web_site_sk:ID(Web_Site)', 'mkt_class', first_sk)
        function.delete_info_for3(df, 'web_site_sk:ID(Web_Site)', 'market_manager', first_sk)
        function.delete_info_for3(df, 'web_site_sk:ID(Web_Site)', 'tax_percentage', first_sk)
        function.delete_info_for1(df, 'web_site_sk:ID(Web_Site)', 'street_number', first_sk)
        function.delete_info_for1(df, 'web_site_sk:ID(Web_Site)', 'street_name', first_sk)
        function.delete_info_for1(df, 'web_site_sk:ID(Web_Site)', 'street_type', first_sk)
        function.delete_info_for1(df, 'web_site_sk:ID(Web_Site)', 'suite_number', first_sk)
        function.delete_info_for1(df, 'web_site_sk:ID(Web_Site)', 'city', first_sk)
        function.delete_info_for1(df, 'web_site_sk:ID(Web_Site)', 'county', first_sk)
        function.delete_info_for1(df, 'web_site_sk:ID(Web_Site)', 'state', first_sk)
        function.delete_info_for1(df, 'web_site_sk:ID(Web_Site)', 'zip', first_sk)
        function.delete_info_for1(df, 'web_site_sk:ID(Web_Site)', 'country', first_sk)
        function.delete_info_for1(df, 'web_site_sk:ID(Web_Site)', 'gmt_offset', first_sk)

        # add information for 3er evolution
        df['manager'][df['web_site_sk:ID(Web_Site)'] == last_sk] = information['manager']
        date = pd.to_datetime(information['start_valid_time']) + pd.Timedelta(days=365)
        df.loc[last_sk - 1, 'end_valid_time'] = date[last_sk - 1].strftime("%Y-%m-%d")

        # 4er evolution
        # evolution schema
        # add information for 1er instance
        information_new = df[df['web_site_sk:ID(Web_Site)'] == last_sk]
        # change sk
        newSk = len(df) + 1
        information_new['web_site_sk:ID(Web_Site)'] = newSk
        # add information
        information_new['mkt_id'] = information['mkt_id']
        information_new['mkt_class'] = 'NoValue'
        information_new['market_manager'] = 'NoValue'
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateSchemaSchema(df, 'web_site_sk:ID(Web_Site)', newSk)

        # add information for 2er instance
        # evolution instance
        information_new = df[df['web_site_sk:ID(Web_Site)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['web_site_sk:ID(Web_Site)'] = newSk
        # add information
        information_new['mkt_id'] = information['mkt_id']
        information_new['mkt_class'] = information['mkt_class']
        information_new['market_manager'] = 'NoValue'
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateSchemaInstance(df, 'web_site_sk:ID(Web_Site)', newSk)

        # add information for 3er instance
        # evolution instance
        information_new = df[df['web_site_sk:ID(Web_Site)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['web_site_sk:ID(Web_Site)'] = newSk
        # add information
        information_new['mkt_id'] = information['mkt_id']
        information_new['mkt_class'] = information['mkt_class']
        information_new['market_manager'] = information['market_manager']
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateInstanceInstance(df, 'web_site_sk:ID(Web_Site)', newSk)

        # 5er evolution
        # evolution schema
        # add information for 1er instance
        information_new = df[df['web_site_sk:ID(Web_Site)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['web_site_sk:ID(Web_Site)'] = newSk
        # add information
        information_new['tax_percentage'] = 'NoValue'
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateInstanceSchema(df, 'web_site_sk:ID(Web_Site)', newSk)

        # add information for 2er instance
        # evolution instance
        information_new = df[df['web_site_sk:ID(Web_Site)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['web_site_sk:ID(Web_Site)'] = newSk
        # add information
        information_new['tax_percentage'] = information['tax_percentage']
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateSchemaInstance(df, 'web_site_sk:ID(Web_Site)', newSk)

new_path = os.path.join(path_1, "web_site.csv")
df.to_csv(new_path, index=False, sep=",")

# transfer promotion
dir_path = os.path.join(path_0, "promotion.dat")
df = pd.read_table(dir_path, sep="|", header=None)

df = df.iloc[:, [0, 1, 2, 3, 4, 5, 7, 8, 16, 18]]
df.columns = ["promo_sk:ID(Promotion)", "promo_id", "date_sk", "end_date_sk", "item_sk", "cost", "promo_name",
              "channel_dmail", "channel_details", "discount_active"]

df = df.fillna(0)
df['item_sk'] = df['item_sk'].astype(int)

df = pd.merge(df, dfDate, on="date_sk")
df = df.drop(columns=["date_sk"])
df.rename(columns={'date': 'start_date_sk'}, inplace=True)
df.rename(columns={'end_date_sk': 'date_sk'}, inplace=True)
df = pd.merge(df, dfDate, on="date_sk")
df = df.drop(columns=["date_sk"])
df.rename(columns={'date': 'end_date_sk'}, inplace=True)

pd.to_datetime(df['start_date_sk'])
pd.to_datetime(df['end_date_sk'])

df[':LABEL'] = 'Promotion'

# Count line
nb_df = len(df)
# create a dataframe only with business key
df_evo = df['promo_id']
# delete repetition key
df_evo = df_evo.unique()
nb_evo = len(df_evo)

for i in df_evo:
    nb = 0
    for j in df['promo_id']:
        if i == j:
            nb += 1
    if nb == 2:
        sk = df.loc[df['promo_id'] == i].index.tolist()
        first_sk = sk[0] + 1
        # collect and delete information
        last_sk = sk[1] + 1
        information = df[df['promo_sk:ID(Promotion)'] == last_sk]
        # delete information
        function.delete_info_for2(df, 'promo_sk:ID(Promotion)', 'discount_active', first_sk)
        function.delete_info_for1(df, 'promo_sk:ID(Promotion)', 'channel_dmail', first_sk)
        function.delete_info_for1(df, 'promo_sk:ID(Promotion)', 'channel_details', first_sk)
        # evolution for 3er instance
        newSk = len(df) + 1
        # add information
        information_new = df[df['promo_sk:ID(Promotion)'] == last_sk]
        # change sk
        information_new['promo_sk:ID(Promotion)'] = newSk
        # add information
        information_new['discount_active'] = information['discount_active']
        # change dates
        date = pd.to_datetime(information['start_valid_time']) + pd.Timedelta(days=365)
        # change date of last tuple
        df.loc[last_sk-1, 'end_valid_time'] = date[last_sk-1].strftime("%Y-%m-%d")
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateSchemaSchema(df, 'promo_sk:ID(Promotion)', newSk)
    elif nb == 3:
        sk = df.loc[df['promo_id'] == i].index.tolist()
        first_sk = sk[0] + 1
        # collect and delete information
        last_sk = sk[2] + 1
        information = df[df['promo_sk:ID(Promotion)'] == last_sk]
        # delete information
        function.delete_info_for2(df, 'promo_sk:ID(Promotion)', 'discount_active', first_sk)
        function.delete_info_for1(df, 'promo_sk:ID(Promotion)', 'channel_dmail', first_sk)
        function.delete_info_for1(df, 'promo_sk:ID(Promotion)', 'channel_details', first_sk)

        # add information for 3er evolution
        df['discount_active'][df['promo_sk:ID(Promotion)'] == last_sk] = information['discount_active']
        date = pd.to_datetime(information['start_valid_time']) + pd.Timedelta(days=365)
        df.loc[last_sk - 1, 'end_valid_time'] = date[last_sk - 1].strftime("%Y-%m-%d")

new_path = os.path.join(path_1, "promotion.csv")
df.to_csv(new_path, index=False, sep=",")


# transfer catalog_page
dir_path = os.path.join(path_0, "catalog_page.dat")
df = pd.read_table(dir_path, sep="|", header=None)

# add names of columns
df.columns = ["catalog_page_sk:ID(Catalog_Page)", "catalog_page_id", "date_sk", "end_date_sk", "department",
              "catalog_number", "catalog_page_number", "description", "type", ":LABEL"]

df = df.fillna(0)
df['catalog_number'] = df['catalog_number'].astype(int)
df['catalog_page_number'] = df['catalog_page_number'].astype(int)

# Change the information about the date column
df = pd.merge(df, dfDate, on="date_sk")
df = df.drop(columns=["date_sk"])
df.rename(columns={'date': 'start_date_sk'}, inplace=True)
df.rename(columns={'end_date_sk': 'date_sk'}, inplace=True)
df = pd.merge(df, dfDate, on="date_sk")
df = df.drop(columns=["date_sk"])
df.rename(columns={'date': 'end_date_sk'}, inplace=True)

pd.to_datetime(df['start_date_sk'])
pd.to_datetime(df['end_date_sk'])

df[':LABEL'] = 'Catalog_Page'

# Count line
nb_df = len(df)
# create a dataframe only with business key
df_evo = df['catalog_page_id']
# delete repetition key
df_evo = df_evo.unique()
nb_evo = len(df_evo)

for i in df_evo:
    nb = 0
    for j in df['catalog_page_id']:
        if i == j:
            nb += 1
    if nb == 2:
        sk = df.loc[df['catalog_page_id'] == i].index.tolist()
        first_sk = sk[0] + 1
        # collect and delete information
        last_sk = sk[1] + 1
        information = df[df['catalog_page_sk:ID(Catalog_Page)'] == last_sk]
        # delete information
        function.delete_info_for1(df, 'catalog_page_sk:ID(Catalog_Page)', 'description', first_sk)
        function.delete_info_for1(df, 'catalog_page_sk:ID(Catalog_Page)', 'type', first_sk)
        # evolution for 3er instance
        newSk = len(df) + 1
        # add information
        information_new = df[df['catalog_page_sk:ID(Catalog_Page)'] == last_sk]
        # change sk
        information_new['catalog_page_sk:ID(Catalog_Page)'] = newSk
        # add information
        information_new['type'] = information['type']
        # change dates
        date = pd.to_datetime(information['start_valid_time']) + pd.Timedelta(days=365)
        # change date of last tuple
        df.loc[last_sk-1, 'end_valid_time'] = date[last_sk-1].strftime("%Y-%m-%d")
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateSchemaSchema(df, 'catalog_page_sk:ID(Catalog_Page)', newSk)
    elif nb == 3:
        sk = df.loc[df['catalog_page_id'] == i].index.tolist()
        first_sk = sk[0] + 1
        # collect and delete information
        last_sk = sk[2] + 1
        information = df[df['catalog_page_sk:ID(Catalog_Page)'] == last_sk]
        # delete information
        function.delete_info_for2(df, 'catalog_page_sk:ID(Catalog_Page)', 'description', first_sk)
        function.delete_info_for1(df, 'catalog_page_sk:ID(Catalog_Page)', 'type', first_sk)

        # add information for 3er evolution
        df['type'][df['catalog_page_sk:ID(Catalog_Page)'] == last_sk] = information['type']
        date = pd.to_datetime(information['start_valid_time']) + pd.Timedelta(days=365)
        df.loc[last_sk - 1, 'end_valid_time'] = date[last_sk - 1].strftime("%Y-%m-%d")

# create csv
new_path = os.path.join(path_1, "catalog_page.csv")
df.to_csv(new_path, index=False, sep=",")


# transfer item
dir_path = os.path.join(path_0, "item.dat")
df = pd.read_table(dir_path, sep="|", header=None)

df.columns = ["item_sk:ID(Item)", "item_id", "start_valid_time", "end_valid_time", "item_desc", "current_price",
              "wholesale_cost", "brand_id", "brand", "class_id", "class", "category_id", "category", "manufact_id",
              "manufact", "size", "formulation", "color", "units", "container", "manager_id", "product_name", ":LABEL"]

pd.to_datetime(df['start_valid_time'])
pd.to_datetime(df['end_valid_time'])

df[':LABEL'] = 'Item'
df = df.drop(columns=["container"])

# Count line
nb_df = len(df)
# create a dataframe only with business key
df_evo = df['item_id']
# delete repetition key
df_evo = df_evo.unique()
# select sample
nb_entity = len(df_evo)
# fix a percentage
percentage = 0.1
nb_sample = int(nb_entity * percentage)
print(nb_sample)

df_evo = np.random.choice(df_evo, nb_sample, replace=False)

print(df_evo)
time_start = time.time()

for i in df_evo:
    nb = 0
    for j in df['item_id']:
        if i == j:
            nb += 1
    if nb == 2:
        sk = df.loc[df['item_id'] == i].index.tolist()
        first_sk = sk[0] + 1
        # collect and delete information
        last_sk = sk[1] + 1
        information = df[df['item_sk:ID(Item)'] == last_sk]
        # delete information
        function.delete_info_for2(df, 'item_sk:ID(Item)', 'item_desc', first_sk)
        function.delete_info_for2(df, 'item_sk:ID(Item)', 'brand_id', first_sk)
        function.delete_info_for2(df, 'item_sk:ID(Item)', 'brand', first_sk)
        function.delete_info_for2(df, 'item_sk:ID(Item)', 'class_id', first_sk)
        function.delete_info_for2(df, 'item_sk:ID(Item)', 'class', first_sk)
        function.delete_info_for2(df, 'item_sk:ID(Item)', 'category_id', first_sk)
        function.delete_info_for2(df, 'item_sk:ID(Item)', 'category', first_sk)
        function.delete_info_for2(df, 'item_sk:ID(Item)', 'manufact_id', first_sk)
        function.delete_info_for2(df, 'item_sk:ID(Item)', 'manufact', first_sk)
        function.delete_info_for2(df, 'item_sk:ID(Item)', 'formulation', first_sk)
        function.delete_info_for2(df, 'item_sk:ID(Item)', 'color', first_sk)
        function.delete_info_for2(df, 'item_sk:ID(Item)', 'units', first_sk)
        function.delete_info_for2(df, 'item_sk:ID(Item)', 'manager_id', first_sk)
        function.delete_info_for1(df, 'item_sk:ID(Item)', 'units', first_sk)
        function.delete_info_for1(df, 'item_sk:ID(Item)', 'size', first_sk)
        # 3er evolution
        # evolution schema
        newSk = len(df) + 1
        # add information
        information_new = df[df['item_sk:ID(Item)'] == last_sk]
        # change sk
        information_new['item_sk:ID(Item)'] = newSk
        # add information
        information_new['color'] = information['color']
        # change dates
        date = pd.to_datetime(information['start_valid_time']) + pd.Timedelta(days=30)
        if pd.isnull(date[last_sk-1]):
            information_new.start_valid_time = df['end_valid_time'][df['item_sk:ID(Item)'] == (last_sk-1)].values
            information.start_valid_time = df['end_valid_time'][df['item_sk:ID(Item)'] == (last_sk - 1)].values
            df['start_valid_time'][df['item_sk:ID(Item)'] == last_sk] = \
                df['end_valid_time'][df['item_sk:ID(Item)'] == (last_sk - 1)].values
            date = pd.to_datetime(information['start_valid_time']) + pd.Timedelta(days=30)
        df.loc[last_sk - 1, 'end_valid_time'] = date[last_sk - 1].strftime("%Y-%m-%d")
        df = df.append(information_new)
        function.changeDateSchemaSchema(df, 'item_sk:ID(Item)', newSk)

        # 4er evolution
        # evolution schema
        # add information for 1er instance
        information_new = df[df['item_sk:ID(Item)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['item_sk:ID(Item)'] = newSk
        # add information
        information_new['formulation'] = 'NoValue'
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateSchemaSchema(df, 'item_sk:ID(Item)', newSk)

        # add information for 2er instance
        # evolution instance
        information_new = df[df['item_sk:ID(Item)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['item_sk:ID(Item)'] = newSk
        # add information
        information_new['formulation'] = information['formulation']
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateSchemaInstance(df, 'item_sk:ID(Item)', newSk)

        # 5er evolution
        # evolution schema
        # add information for 1er instance
        information_new = df[df['item_sk:ID(Item)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['item_sk:ID(Item)'] = newSk
        # add information
        information_new['brand_id'] = 'NoValue'
        information_new['brand'] = information['brand']
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateInstanceSchema(df, 'item_sk:ID(Item)', newSk)

        # add information 2er instance
        # evolution instance
        information_new = df[df['item_sk:ID(Item)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['item_sk:ID(Item)'] = newSk
        # add information
        information_new['brand_id'] = information['brand_id']
        information_new['brand'] = information['brand']
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateSchemaInstance(df, 'item_sk:ID(Item)', newSk)

        # 6er evolution
        # evolution schema
        # add information for 1er instance
        information_new = df[df['item_sk:ID(Item)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['item_sk:ID(Item)'] = newSk
        # add information
        information_new['class_id'] = information['class_id']
        information_new['class'] = information['class']
        information_new['category_id'] = 'NoValue'
        information_new['category'] = 'NoValue'
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateInstanceSchema(df, 'item_sk:ID(Item)', newSk)

        # add information for 2er instance
        # evolution instance
        information_new = df[df['item_sk:ID(Item)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['item_sk:ID(Item)'] = newSk
        # add information
        information_new['class_id'] = information['class_id']
        information_new['class'] = information['class']
        information_new['category_id'] = information['category_id']
        information_new['category'] = information['category']
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateSchemaInstance(df, 'item_sk:ID(Item)', newSk)

        # 7er evolution
        # evolution schema
        # add information for 1er instance
        information_new = df[df['item_sk:ID(Item)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['item_sk:ID(Item)'] = newSk
        # add information
        information_new['manufact_id'] = information['manufact_id']
        information_new['manufact'] = information['manufact']
        information_new['manager_id'] = 'NoValue'
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateInstanceSchema(df, 'item_sk:ID(Item)', newSk)

        # add information 2er instance
        # evolution instance
        information_new = df[df['item_sk:ID(Item)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['item_sk:ID(Item)'] = newSk
        # add information
        information_new['manufact_id'] = information['manufact_id']
        information_new['manufact'] = information['manufact']
        information_new['manager_id'] = information['manager_id']
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateSchemaInstance(df, 'item_sk:ID(Item)', newSk)
    elif nb == 3:
        sk = df.loc[df['item_id'] == i].index.tolist()
        first_sk = sk[0] + 1
        # collect and delete information
        last_sk = sk[2] + 1
        information = df[df['item_sk:ID(Item)'] == last_sk]
        # delete information
        function.delete_info_for3(df, 'item_sk:ID(Item)', 'item_desc', first_sk)
        function.delete_info_for3(df, 'item_sk:ID(Item)', 'brand_id', first_sk)
        function.delete_info_for3(df, 'item_sk:ID(Item)', 'brand', first_sk)
        function.delete_info_for3(df, 'item_sk:ID(Item)', 'class_id', first_sk)
        function.delete_info_for3(df, 'item_sk:ID(Item)', 'class', first_sk)
        function.delete_info_for3(df, 'item_sk:ID(Item)', 'category_id', first_sk)
        function.delete_info_for3(df, 'item_sk:ID(Item)', 'category', first_sk)
        function.delete_info_for3(df, 'item_sk:ID(Item)', 'manufact_id', first_sk)
        function.delete_info_for3(df, 'item_sk:ID(Item)', 'manufact', first_sk)
        function.delete_info_for3(df, 'item_sk:ID(Item)', 'formulation', first_sk)
        function.delete_info_for3(df, 'item_sk:ID(Item)', 'color', first_sk)
        function.delete_info_for3(df, 'item_sk:ID(Item)', 'units', first_sk)
        function.delete_info_for3(df, 'item_sk:ID(Item)', 'manager_id', first_sk)
        function.delete_info_for1(df, 'item_sk:ID(Item)', 'units', first_sk)
        function.delete_info_for1(df, 'item_sk:ID(Item)', 'size', first_sk)

        # add information for 3er evolution
        df['color'][df['item_sk:ID(Item)'] == last_sk] = information['color']
        # change dates
        date = pd.to_datetime(information['start_valid_time']) + pd.Timedelta(days=365)
        if pd.isnull(date[last_sk-1]):
            print(i)
            information.start_valid_time = df['end_valid_time'][df['item_sk:ID(Item)'] == (last_sk - 1)].values
            information_new.start_valid_time = df['end_valid_time'][df['item_sk:ID(Item)'] == (last_sk-1)].values
            df['start_valid_time'][df['item_sk:ID(Item)'] == last_sk] = \
                df['end_valid_time'][df['item_sk:ID(Item)'] == (last_sk-1)].values
            date = pd.to_datetime(information['start_valid_time']) + pd.Timedelta(days=365)
        df.loc[last_sk - 1, 'end_valid_time'] = date[last_sk - 1].strftime("%Y-%m-%d")

        # 4er evolution
        # evolution schema
        # add information for 1er instance
        information_new = df[df['item_sk:ID(Item)'] == last_sk]
        # change sk
        newSk = len(df) + 1
        information_new['item_sk:ID(Item)'] = newSk
        # add information
        information_new['formulation'] = 'NoValue'
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateSchemaSchema(df, 'item_sk:ID(Item)', newSk)

        # add information for 2er instance
        # evolution instance
        information_new = df[df['item_sk:ID(Item)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['item_sk:ID(Item)'] = newSk
        # add information
        information_new['formulation'] = information['formulation']
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateSchemaInstance(df, 'item_sk:ID(Item)', newSk)

        # 5er evolution
        # evolution schema
        # add information for 1er instance
        information_new = df[df['item_sk:ID(Item)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['item_sk:ID(Item)'] = newSk
        # add information
        information_new['brand_id'] = 'NoValue'
        information_new['brand'] = information['brand']
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateInstanceSchema(df, 'item_sk:ID(Item)', newSk)

        # add information for 2er instance
        # evolution instance
        information_new = df[df['item_sk:ID(Item)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['item_sk:ID(Item)'] = newSk
        # add information
        information_new['brand_id'] = information['brand_id']
        information_new['brand'] = information['brand']
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateSchemaInstance(df, 'item_sk:ID(Item)', newSk)

        # 6er evolution
        # evolution schema
        # add information for 1er instance
        information_new = df[df['item_sk:ID(Item)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['item_sk:ID(Item)'] = newSk
        # add information
        information_new['class_id'] = information['class_id']
        information_new['class'] = information['class']
        information_new['category_id'] = 'NoValue'
        information_new['category'] = 'NoValue'
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateInstanceSchema(df, 'item_sk:ID(Item)', newSk)

        # add information for 2er instance
        # evolution instance
        information_new = df[df['item_sk:ID(Item)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['item_sk:ID(Item)'] = newSk
        # add information
        information_new['class_id'] = information['class_id']
        information_new['class'] = information['class']
        information_new['category_id'] = information['category_id']
        information_new['category'] = information['category']
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateSchemaInstance(df, 'item_sk:ID(Item)', newSk)

        # 7er evolution
        # evolution schema
        # add information for 1er instance
        information_new = df[df['item_sk:ID(Item)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['item_sk:ID(Item)'] = newSk
        # add information
        information_new['manufact_id'] = information['manufact_id']
        information_new['manufact'] = information['manufact']
        information_new['manager_id'] = 'NoValue'
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateInstanceSchema(df, 'item_sk:ID(Item)', newSk)

        # add information for 2er instance
        # evolution instance
        information_new = df[df['item_sk:ID(Item)'] == newSk]
        # change sk
        newSk = len(df) + 1
        information_new['item_sk:ID(Item)'] = newSk
        # add information
        information_new['manufact_id'] = information['manufact_id']
        information_new['manufact'] = information['manufact']
        information_new['manager_id'] = information['manager_id']
        # add information for new tuples
        df = df.append(information_new)
        # change date
        function.changeDateSchemaInstance(df, 'item_sk:ID(Item)', newSk)


# create has_household_demographics
pd.set_option('mode.chained_assignment', None)
dir_path = os.path.join(path_0, "household_demographics.dat")
hd = pd.read_table(dir_path, sep="|", header=None, encoding='latin-1')
hd.columns = ["current_hdemo_sk", "income_band_sk", "buy_potential",
              "dep_count", "vehicle_count", ":TYPE"]
pd.set_option('mode.chained_assignment', None)
dir_path = os.path.join(path_0, "household_demographics.dat")
hd = pd.read_table(dir_path, sep="|", header=None, encoding='latin-1')
hd.columns = ["current_hdemo_sk", "income_band_sk", "buy_potential",
              "dep_count", "vehicle_count", ":TYPE"]

df = pd.merge(df, hd, on="current_hdemo_sk")

df_relation = pd.DataFrame(df, columns=['customer_sk:ID(Customer)', 'buy_potential', 'dep_count',
                                        'vehicle_count', 'last_review_date', 'current_hdemo_sk'])
df_relation.insert(4, 'start_valid_time', df_relation['last_review_date'])

df_relation.columns = [":START_ID(Customer)", 'buy_potential', 'dep_count',
                       'vehicle_count', 'start_valid_time', 'end_valid_time', ':END_ID(Household_Demographics)']

df_relation[':TYPE'] = 'Has_Household_Demographics'
nb = 1
nbSample = int(len(df_relation)*0.1)
print(df_relation)
df_relation = df_relation.sample(nbSample)
print(df_relation)
for index, row in df_relation.iterrows():
    # collect the information
    sk = row[':START_ID(Customer)']
    # collect the start valid time
    date = pd.to_datetime(df_relation['start_valid_time'][df_relation[':START_ID(Customer)'] == sk]).values[0]
    # change time for end valid time
    date = date + pd.Timedelta(days=365)
    information = df_relation[df_relation[':START_ID(Customer)'] == sk]
    df_relation['end_valid_time'][df_relation[':START_ID(Customer)'] == sk] = date.strftime("%Y-%m-%d")
    # delete the information for 1st instance of 1st evolution
    df_relation['buy_potential'][df_relation[':START_ID(Customer)'] == sk] = 'NULL'
    df_relation['dep_count'][df_relation[':START_ID(Customer)'] == sk] = None #to enable the non importation of the attribute
    df_relation['vehicle_count'][df_relation[':START_ID(Customer)'] == sk] = None #to enable the non importation of the attribute

    # add for 2nd instance of 1st evolution
    information_new = df_relation[df_relation[':START_ID(Customer)'] == sk]
    information_new['buy_potential'] = information['buy_potential']
    date = date + pd.Timedelta(days=1)
    information_new['start_valid_time'] = date.strftime("%Y-%m-%d")
    date = date + pd.Timedelta(days=30)
    information_new['end_valid_time'] = date.strftime("%Y-%m-%d")
    df_relation = df_relation.append(information_new)

    # add for 1st instance of 2nd evolution
    information_new['dep_count'] = 'NULL'
    date = date + pd.Timedelta(days=1)
    information_new['start_valid_time'] = date.strftime("%Y-%m-%d")
    date = date + pd.Timedelta(days=365)
    information_new['end_valid_time'] = date.strftime("%Y-%m-%d")
    df_relation = df_relation.append(information_new)

    # add for 2nd instance of 2nd evolution
    information_new['dep_count'] = information['dep_count']
    date = date + pd.Timedelta(days=1)
    information_new['start_valid_time'] = date.strftime("%Y-%m-%d")
    date = date + pd.Timedelta(days=30)
    information_new['end_valid_time'] = date.strftime("%Y-%m-%d")
    df_relation = df_relation.append(information_new)

    # add for 1st instance of 3rd evolution
    information_new['vehicle_count'] = 'NULL'
    date = date + pd.Timedelta(days=1)
    information_new['start_valid_time'] = date.strftime("%Y-%m-%d")
    date = date + pd.Timedelta(days=365)
    information_new['end_valid_time'] = date.strftime("%Y-%m-%d")
    df_relation = df_relation.append(information_new)

    # add for 2nd instance of 3rd evolution
    information_new['vehicle_count'] = information['vehicle_count']
    date = date + pd.Timedelta(days=1)
    information_new['start_valid_time'] = date.strftime("%Y-%m-%d")
    date = date + pd.Timedelta(days=30)
    information_new['end_valid_time'] = date.strftime("%Y-%m-%d")
    df_relation = df_relation.append(information_new)
    print(nb)
    nb += 1
    # if nb == 201:
    #     break
# add the information of sk in relationship
df_relation.insert(1, 'customer_sk', df_relation[':START_ID(Customer)'])
df_relation.insert(2, 'current_hdemo_sk', df_relation[':END_ID(Household_Demographics)'])
new_path = os.path.join(path_1, "has_household_demographics.csv")
df_relation.to_csv(new_path, index=False, sep=",") #create the csv file of household_demographics



time_end = time.time()
print('time cost', time_end - time_start, 's')
new_path = os.path.join(path_1, "item.csv")
df.to_csv(new_path, index=False, sep=",")
