#Run on the Virtual Machine with OrientDB


# this model in order to run the query of orientDB and count the execution time
from pyorient.ogm import Graph, Config
import pandas as pd
import numpy
import time



# count the time of running the program
startGeneral = time.time()


# database chosen
database = '1gb'
# read csv of query
# where we find the csv of query
path_0 = '/home/activus01/data/queryOrientDB1gb.csv'
dfQuery = pd.read_csv(path_0, sep=",")

# Connect to Database
graph = Graph(
      Config.from_url('localhost/'+database, 'root', '*******'))

path_1 = '/home/activus01/data/'
path_1 = path_1 + 'ResultOrientDB' + str(database) + '.csv'
# create new result frame
dfResult = pd.DataFrame(columns=["Query", "result"])

for index, row in dfQuery.iterrows():
    # get the query one by one
    query = row[1]
    print query
    # for each query we execute 10 times
    # initialization of list and number of runs
    i = 1
    time_list = []
    while i < 11:
        # every times we count the execution time
        time_start = time.time()
        data = graph.client.command(query)
        time_end = time.time()
        time_run = time_end - time_start
        time_list.append(time_run)
        i = i + 1
    # delete max and min time of list
    time_list.remove(max(time_list))
    time_list.remove(max(time_list))
    time_final = round(numpy.mean(time_list), 5)
    # add in result
    addline = pd.DataFrame(columns=dfResult.columns)
    addline = addline.append({"Query": row[0], "result": time_final}, ignore_index=True)
    addline.to_csv(path_1, index=False, sep=",", mode='a', header=False)

endGeneral = time.time()
print('time cost', endGeneral - startGeneral, 's')

# shutdown the database
graph.client.shutdown()

endGeneral = time.time()
print('time cost', endGeneral - startGeneral, 's')
