import pandas as pd

#To modify the file has_household_demographics.csv for each data volume, change the path directories of the variables input_file, hdemo_file and output_file to the corresponding dataset.
#read raw data
input_file = '/home/activus/data/data1gb/has_household_demographics.csv' #file obtained from evolution.py
hdemo_file = '/home/activus/data/data1gb/household_demographics.csv'
output_file = '/home/activus/data/data1gb/has_household_demographics_CLEAN.csv'

#read has_household_demographics.csv
df_hhd = pd.read_csv(input_file, sep=",")
#rename ':END_ID(Household_Demographics)'
df_hhd.rename(columns={':END_ID(Household_Demographics)':':END_ID(Income_Band)'}, inplace=True)
print(df_hhd.head())
#read household_demographics.csv
df_hd =pd.read_csv(hdemo_file, sep=",")
#rename 'hd_demo_sk:ID(Household_Demographics)' to merge with hhd
df_hd.rename(columns={'hd_demo_sk:ID(Household_Demographics)':'current_hdemo_sk'}, inplace=True)
print(df_hd.head())
#only select some columns of household_demographics
df_hd=df_hd[['current_hdemo_sk', 'income_band_sk']].copy()
print(df_hd.head())
df_merge = df_hhd.merge(df_hd, on="current_hdemo_sk", how='left')

print(df_merge.head())
df_merge[':END_ID(Income_Band)'] = df_merge['income_band_sk']
print(df_merge.head())
df_final = df_merge[[':START_ID(Customer)','customer_sk', 'buy_potential','dep_count','vehicle_count', 'start_valid_time', 'end_valid_time',':END_ID(Income_Band)', ':TYPE', 'income_band_sk']]
df_final.head()
df_final.to_csv(output_file, index=False, sep=',')
