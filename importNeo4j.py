# Run on the Virtual Machine with Neo4j

import os

# remove csv in file import
passWord = '*****'
cmd = 'sudo rm /var/lib/neo4j/import/*.csv'
# os.system(cmd) execute the command as in the terminal
# enter the password automatically
os.system('echo %s|sudo -S %s' % (passWord, cmd)) 

# copy csv of the volume you want to import ---- modify with data1GB, data2GB, data4GB, data8GB 
cmd = 'sudo cp /home/activus/data/data1GB/*.csv /var/lib/neo4j/import'
os.system('echo %s|sudo -S %s' % (passWord, cmd))

# clean database of the volume you want to import --- modify with graph2, graph4, graph8
cmd = 'sudo rm /var/lib/neo4j/data/databases/graph1.db/*'
os.system('echo %s|sudo -S %s' % (passWord, cmd))

# stop neo4j server
cmd = 'cd /var/lib/neo4j/'
os.system(cmd)
cmd = 'sudo neo4j stop'
os.system('echo %s|sudo -S %s' % (passWord, cmd))


# import data --- modify graph2, graph4, graph8
cmd = 'cd /var/lib/neo4j/'
os.system(cmd) 
cmd = 'sudo neo4j-admin import --database=graph1.db ' \
      '--nodes=/var/lib/neo4j/import/call_center.csv ' \
      '--nodes=/var/lib/neo4j/import/catalog_page.csv ' \
      '--nodes=/var/lib/neo4j/import/store.csv ' \
      '--nodes=/var/lib/neo4j/import/customer.csv ' \
      '--nodes=/var/lib/neo4j/import/customer_address.csv ' \
      '--nodes=/var/lib/neo4j/import/customer_demographics.csv ' \
      '--nodes=/var/lib/neo4j/import/household_demographics.csv ' \
      '--nodes=/var/lib/neo4j/import/income_band.csv ' \
      '--nodes=/var/lib/neo4j/import/promotion.csv ' \
      '--nodes=/var/lib/neo4j/import/ship_mode.csv ' \
      '--nodes=/var/lib/neo4j/import/item.csv ' \
      '--nodes=/var/lib/neo4j/import/warehouse.csv ' \
      '--nodes=/var/lib/neo4j/import/web_page.csv ' \
      '--nodes=/var/lib/neo4j/import/web_site.csv ' \
      '--nodes=/var/lib/neo4j/import/reason.csv ' \
      '--relationships=/var/lib/neo4j/import/cr_item.csv ' \
      '--relationships=/var/lib/neo4j/import/cr_customer_item.csv ' \
      '--relationships=/var/lib/neo4j/import/cr_call_center.csv ' \
      '--relationships=/var/lib/neo4j/import/cs_call_center.csv ' \
      '--relationships=/var/lib/neo4j/import/cs_promotion.csv ' \
      '--relationships=/var/lib/neo4j/import/cs_customer.csv ' \
      '--relationships=/var/lib/neo4j/import/cs_item.csv ' \
      '--relationships=/var/lib/neo4j/import/cs_customer_item.csv ' \
      '--relationships=/var/lib/neo4j/import/has_customer_address.csv ' \
      '--relationships=/var/lib/neo4j/import/has_customer_demographics.csv ' \
      '--relationships=/var/lib/neo4j/import/has_household_demographics.csv ' \
      '--relationships=/var/lib/neo4j/import/has_income_band.csv ' \
      '--relationships=/var/lib/neo4j/import/sr_item.csv ' \
      '--relationships=/var/lib/neo4j/import/sr_customer.csv ' \
      '--relationships=/var/lib/neo4j/import/sr_customer_item.csv ' \
      '--relationships=/var/lib/neo4j/import/ss_customer.csv ' \
      '--relationships=/var/lib/neo4j/import/ss_item.csv ' \
      '--relationships=/var/lib/neo4j/import/ss_customer_item.csv ' \
      '--relationships=/var/lib/neo4j/import/ss_promotion.csv ' \
      '--relationships=/var/lib/neo4j/import/promotion_item.csv ' \
      '--relationships=/var/lib/neo4j/import/wr_customer.csv ' \
      '--relationships=/var/lib/neo4j/import/wr_item.csv ' \
      '--relationships=/var/lib/neo4j/import/wr_customer_item.csv ' \
      '--relationships=/var/lib/neo4j/import/ws_item.csv ' \
      '--relationships=/var/lib/neo4j/import/ws_customer.csv ' \
      '--relationships=/var/lib/neo4j/import/ws_customer_item.csv ' \
      '--relationships=/var/lib/neo4j/import/ws_promotion.csv ' \
      '--relationships=/var/lib/neo4j/import/ws_web_site_web_page.csv ' \
      '--ignore-missing-nodes=true'

os.system('echo %s|sudo -S %s' % (passWord, cmd))

# start server
cmd = 'cd /var/lib/neo4j/'
os.system(cmd)
cmd = 'sudo neo4j start'
os.system('echo %s|sudo -S %s' % (passWord, cmd))
