#run on the Virtual Machine with ArangoDB

# this module allows to import the csv into arangoDB database
# you need to change the name of database which you want to import
import os
import time

time_start = time.time()

passWord = '*******'
# DB which will be imported
# cant be changed
database = 'data1gb'

# import call_center
cmd = 'arangoimp --server.username root --server.password activus01 --server.database ' + database + \
      ' --file /home/activus/data/' + database + '/call_center.csv --type csv ' \
      '--translate "call_center_sk:ID(Call_Center)=_key" --remove-attribute ":LABEL" ' \
      '--separator "," --create-collection true --create-collection-type document ' \
      '--overwrite true --collection "Call_Center"'
os.system(cmd)

# import catalog_page
cmd = 'arangoimp --server.username root --server.password activus01 --server.database ' + database + \
      ' --file /home/activus/data/' + database + '/catalog_page.csv --type csv ' \
      '--translate "catalog_page_sk:ID(Catalog_Page)=_key" --remove-attribute ":LABEL" ' \
      '--separator "," --create-collection true --create-collection-type document ' \
      '--overwrite true --collection "Catalog_Page"'
os.system(cmd)

# import customer
cmd = 'arangoimp --server.username root --server.password activus01 --server.database ' + database + \
      ' --file /home/activus/data/' + database + '/customer.csv --type csv ' \
      '--translate "customer_sk:ID(Customer)=_key" --remove-attribute ":LABEL" ' \
      '--separator "," --create-collection true --create-collection-type document ' \
      '--overwrite true --collection "Customer"'
os.system(cmd)

# import customer_address
cmd = 'arangoimp --server.username root --server.password activus01 --server.database ' + database + \
      ' --file /home/activus/data/' + database + '/customer_address.csv --type csv ' \
      '--translate "address_sk:ID(Customer_Address)=_key" --remove-attribute ":LABEL" ' \
      '--separator "," --create-collection true --create-collection-type document ' \
      '--overwrite true --collection "Customer_Address"'
os.system(cmd)

# import Customer_Demographics
cmd = 'arangoimp --server.username root --server.password activus01 --server.database ' + database + \
      ' --file /home/activus/data/' + database + '/customer_demographics.csv --type csv ' \
      '--translate "cd_demo_sk:ID(Customer_Demographics)=_key" --remove-attribute ":LABEL" ' \
      '--separator "," --create-collection true --create-collection-type document ' \
      '--overwrite true --collection "Customer_Demographics"'
os.system(cmd)

# import Household_Demographics
cmd = 'arangoimp --server.username root --server.password activus01 --server.database ' + database + \
      ' --file /home/activus/data/' + database + '/household_demographics.csv --type csv ' \
      '--translate "hd_demo_sk:ID(Household_Demographics)=_key" --remove-attribute ":LABEL" ' \
      '--separator "," --create-collection true --create-collection-type document ' \
      '--overwrite true --collection "Household_Demographics"'
os.system(cmd)

# import Income_Band
cmd = 'arangoimp --server.username root --server.password activus01 --server.database ' + database + \
      ' --file /home/activus/data/' + database + '/income_band.csv --type csv ' \
      '--translate "income_band_sk:ID(Income_Band)=_key" --remove-attribute ":LABEL" ' \
      '--separator "," --create-collection true --create-collection-type document ' \
      '--overwrite true --collection "Income_Band"'
os.system(cmd)

# import Item
cmd = 'arangoimp --server.username root --server.password activus01 --server.database ' + database + \
      ' --file /home/activus/data/' + database + '/item.csv --type csv ' \
      '--translate "item_sk:ID(Item)=_key" --remove-attribute ":LABEL" ' \
      '--separator "," --create-collection true --create-collection-type document ' \
      '--overwrite true --collection "Item"'
os.system(cmd)

# import Promotion
cmd = 'arangoimp --server.username root --server.password activus01 --server.database ' + database + \
      ' --file /home/activus/data/' + database + '/promotion.csv --type csv ' \
      '--translate "promo_sk:ID(Promotion)=_key" --remove-attribute ":LABEL" ' \
      '--separator "," --create-collection true --create-collection-type document ' \
      '--overwrite true --collection "Promotion"'
os.system(cmd)

# import Reason
cmd = 'arangoimp --server.username root --server.password activus01 --server.database ' + database + \
      ' --file /home/activus/data/' + database + '/reason.csv --type csv ' \
      '--translate "reason_sk:ID(Reason)=_key" --remove-attribute ":LABEL" ' \
      '--separator "," --create-collection true --create-collection-type document ' \
      '--overwrite true --collection "Reason"'
os.system(cmd)

# import Ship_Mode
cmd = 'arangoimp --server.username root --server.password activus01 --server.database ' + database + \
      ' --file /home/activus/data/' + database + '/ship_mode.csv --type csv ' \
      '--translate "ship_mode_sk:ID(Ship_Mode)=_key" --remove-attribute ":LABEL" ' \
      '--separator "," --create-collection true --create-collection-type document ' \
      '--overwrite true --collection "Ship_Mode"'
os.system(cmd)

# import Store
cmd = 'arangoimp --server.username root --server.password activus01 --server.database ' + database + \
      ' --file /home/activus/data/' + database + '/store.csv --type csv ' \
      '--translate "store_sk:ID(Store)=_key" --remove-attribute ":LABEL" ' \
      '--separator "," --create-collection true --create-collection-type document ' \
      '--overwrite true --collection "Store"'
os.system(cmd)

# import Web_Page
cmd = 'arangoimp --server.username root --server.password activus01 --server.database ' + database + \
      ' --file /home/activus/data/' + database + '/web_page.csv --type csv ' \
      '--translate "web_page_sk:ID(Web_Page)=_key" --remove-attribute ":LABEL" ' \
      '--separator "," --create-collection true --create-collection-type document ' \
      '--overwrite true --collection "Web_Page"'
os.system(cmd)

# import Warehouse
cmd = 'arangoimp --server.username root --server.password activus01 --server.database ' + database + \
      ' --file /home/activus/data/' + database + '/warehouse.csv --type csv ' \
      '--translate "warehouse_sk:ID(Warehouse)=_key" --remove-attribute ":LABEL" ' \
      '--separator "," --create-collection true --create-collection-type document ' \
      '--overwrite true --collection "Warehouse"'
os.system(cmd)

# import Web_Site
cmd = 'arangoimp --server.username root --server.password activus01 --server.database ' + database + \
      ' --file /home/activus/data/' + database + '/web_site.csv --type csv ' \
      '--translate "web_site_sk:ID(Web_Site)=_key" --remove-attribute ":LABEL" ' \
      '--separator "," --create-collection true --create-collection-type document ' \
      '--overwrite true --collection "Web_Site"'
os.system(cmd)

# import cr_call_center
cmd = 'arangoimp --server.username root --server.password activus01 --server.database ' + database + \
       ' --file /home/activus/data/' + database + '/cr_call_center.csv --type csv --remove-attribute ":TYPE" ' \
       '--from-collection-prefix Catalog_Page --to-collection-prefix Call_Center --separator "," ' \
       '--translate ":START_ID(Catalog_Page)=_from" --translate ":END_ID(Call_Center)=_to" ' \
       '--create-collection true --create-collection-type edge --overwrite true ' \
       '--collection "CR_Call_Center"'
os.system(cmd)

# import CR_Customer
cmd = 'arangoimp --server.username root --server.password activus01 --server.database ' + database + \
       ' --file /home/activus/data/' + database + '/cr_customer.csv --type csv --remove-attribute ":TYPE" ' \
       '--from-collection-prefix Catalog_Page --to-collection-prefix Customer --separator "," ' \
       '--translate ":START_ID(Catalog_Page)=_from" --translate ":END_ID(Customer)=_to" ' \
       '--create-collection true --create-collection-type edge --overwrite true ' \
       '--collection "CR_Customer"'
os.system(cmd)

# import CR_Item
cmd = 'arangoimp --server.username root --server.password activus01 --server.database ' + database + \
       ' --file /home/activus/data/' + database + '/cr_item.csv --type csv --remove-attribute ":TYPE"  ' \
       '--from-collection-prefix Catalog_Page --to-collection-prefix Item --separator "," ' \
       '--translate ":START_ID(Catalog_Page)=_from" --translate ":END_ID(Item)=_to" ' \
       '--create-collection true --create-collection-type edge --overwrite true ' \
       '--collection "CR_Item"'
os.system(cmd)

# import CR_Customer_Item
cmd = 'arangoimp --server.username root --server.password activus01 --server.database ' + database + \
       ' --file /home/activus/data/' + database + '/cr_customer_item.csv --type csv --remove-attribute ":TYPE" ' \
       '--from-collection-prefix Customer --to-collection-prefix Item --separator "," ' \
       '--translate ":START_ID(Customer)=_from" --translate ":END_ID(Item)=_to" ' \
       '--create-collection true --create-collection-type edge --overwrite true ' \
       '--collection "CR_Customer_Item"'
os.system(cmd)

# import cs_call_center
cmd = 'arangoimp --server.username root --server.password activus01 --server.database ' + database + \
       ' --file /home/activus/data/' + database + '/cs_call_center.csv --type csv --remove-attribute ":TYPE"  ' \
       '--from-collection-prefix Catalog_Page --to-collection-prefix Call_Center --separator "," ' \
       '--translate ":START_ID(Catalog_Page)=_from" --translate ":END_ID(Call_Center)=_to" ' \
       '--create-collection true --create-collection-type edge --overwrite true ' \
       '--collection "CS_Call_Center"'
os.system(cmd)

# import CS_Customer
cmd = 'arangoimp --server.username root --server.password activus01 --server.database ' + database + \
       ' --file /home/activus/data/' + database + '/cs_customer.csv --type csv --remove-attribute ":TYPE" ' \
       '--from-collection-prefix Catalog_Page --to-collection-prefix Customer --separator "," ' \
       '--translate ":START_ID(Catalog_Page)=_from" --translate ":END_ID(Customer)=_to" ' \
       '--create-collection true --create-collection-type edge --overwrite true ' \
       '--collection "CS_Customer"'
os.system(cmd)

# import CS_Item
cmd = 'arangoimp --server.username root --server.password activus01 --server.database ' + database + \
       ' --file /home/activus/data/' + database + '/cs_item.csv --type csv --remove-attribute ":TYPE" ' \
       '--from-collection-prefix Catalog_Page --to-collection-prefix Item --separator "," ' \
       '--translate ":START_ID(Catalog_Page)=_from" --translate ":END_ID(Item)=_to" ' \
       '--create-collection true --create-collection-type edge --overwrite true ' \
       '--collection "CS_Item"'
os.system(cmd)

# import CS_Customer_Item
cmd = 'arangoimp --server.username root --server.password activus01 --server.database ' + database + \
       ' --file /home/activus/data/' + database + '/cs_customer_item.csv --type csv --remove-attribute ":TYPE" ' \
       '--from-collection-prefix Customer --to-collection-prefix Item --separator "," ' \
       '--translate ":START_ID(Customer)=_from" --translate ":END_ID(Item)=_to" ' \
       '--create-collection true --create-collection-type edge --overwrite true ' \
       '--collection "CS_Customer_Item"'
os.system(cmd)

# import CS_Promotion
cmd = 'arangoimp --server.username root --server.password activus01 --server.database ' + database + \
       ' --file /home/activus/data/' + database + '/cs_promotion.csv --type csv --remove-attribute ":TYPE" ' \
       '--from-collection-prefix Catalog_Page --to-collection-prefix Promotion --separator "," ' \
       '--translate ":START_ID(Catalog_Page)=_from" --translate ":END_ID(Promotion)=_to" ' \
       '--create-collection true --create-collection-type edge --overwrite true ' \
       '--collection "CS_Promotion"'
os.system(cmd)

# import has_customer_address
cmd = 'arangoimp --server.username root --server.password activus01 --server.database ' + database + \
       ' --file /home/activus/data/' + database + '/has_customer_address.csv --type csv --remove-attribute ":TYPE" ' \
       '--from-collection-prefix Customer --to-collection-prefix Customer_Address --separator "," ' \
       '--translate ":START_ID(Customer)=_from" --translate ":END_ID(Customer_Address)=_to" ' \
       '--create-collection true --create-collection-type edge --overwrite true ' \
       '--collection "Has_Customer_Address"'
os.system(cmd)


# import has_Customer_Demographics
cmd = 'arangoimp --server.username root --server.password activus01 --server.database ' + database + \
       ' --file /home/activus/data/' + database + '/has_customer_demographics.csv --type csv --remove-attribute ":TYPE" ' \
       '--from-collection-prefix Customer --to-collection-prefix Customer_Demographics --separator "," ' \
       '--translate ":START_ID(Customer)=_from" --translate ":END_ID(Customer_Demographics)=_to" ' \
       '--create-collection true --create-collection-type edge --overwrite true ' \
       '--collection "Has_Customer_Demographics"'
os.system(cmd)

# import has_Household_Demographics
cmd = 'arangoimp --server.username root --server.password activus01 --server.database ' + database + \
       ' --file /home/activus/data/' + database + '/has_household_demographics.csv --type csv --remove-attribute ":TYPE" ' \
       '--from-collection-prefix Customer --to-collection-prefix Income_Band --separator "," ' \
       '--translate ":START_ID(Customer)=_from" --translate ":END_ID(Income_Band)=_to" ' \
       '--create-collection true --create-collection-type edge --overwrite true ' \
       '--collection "Has_Household_Demographics"'
os.system(cmd)


# import has_income_band
cmd = 'arangoimp --server.username root --server.password activus01 --server.database ' + database + \
       ' --file /home/activus/data/' + database + '/has_income_band.csv --type csv --remove-attribute ":TYPE" ' \
       '--from-collection-prefix Household_Demographics --to-collection-prefix Income_Band --separator "," ' \
       '--translate ":START_ID(Household_Demographics)=_from" --translate ":END_ID(Income_Band)=_to" ' \
       '--create-collection true --create-collection-type edge --overwrite true ' \
       '--collection "Has_Income_Band"'
os.system(cmd)


# import promotion_item
cmd = 'arangoimp --server.username root --server.password activus01 --server.database ' + database + \
       ' --file /home/activus/data/' + database + '/promotion_item.csv --type csv --remove-attribute ":TYPE"  ' \
       '--from-collection-prefix Promotion --to-collection-prefix Item --separator "," ' \
       '--translate ":START_ID(Promotion)=_from" --translate ":END_ID(Item)=_to" ' \
       '--create-collection true --create-collection-type edge --overwrite true ' \
       '--collection "Promotion_Item"'
os.system(cmd)


# import SR_Customer
cmd = 'arangoimp --server.username root --server.password activus01 --server.database ' + database + \
       ' --file /home/activus/data/' + database + '/sr_customer.csv --type csv --remove-attribute ":TYPE" ' \
       '--from-collection-prefix Store --to-collection-prefix Customer --separator "," ' \
       '--translate ":START_ID(Store)=_from" --translate ":END_ID(Customer)=_to" ' \
       '--create-collection true --create-collection-type edge --overwrite true ' \
       '--collection "SR_Customer"'
os.system(cmd)

# import SR_Item
cmd = 'arangoimp --server.username root --server.password activus01 --server.database ' + database + \
       ' --file /home/activus/data/' + database + '/sr_item.csv --type csv --remove-attribute ":TYPE" ' \
       '--from-collection-prefix Store --to-collection-prefix Item --separator "," ' \
       '--translate ":START_ID(Store)=_from" --translate ":END_ID(Item)=_to" ' \
       '--create-collection true --create-collection-type edge --overwrite true ' \
       '--collection "SR_Item"'
os.system(cmd)

# import SR_Customer_Item
cmd = 'arangoimp --server.username root --server.password activus01 --server.database ' + database + \
       ' --file /home/activus/data/' + database + '/sr_customer_item.csv --type csv --remove-attribute ":TYPE" ' \
       '--from-collection-prefix Customer --to-collection-prefix Item --separator "," ' \
       '--translate ":START_ID(Customer)=_from" --translate ":END_ID(Item)=_to" ' \
       '--create-collection true --create-collection-type edge --overwrite true ' \
       '--collection "SR_Customer_Item"'
os.system(cmd)

# import SS_Customer
cmd = 'arangoimp --server.username root --server.password activus01 --server.database ' + database + \
       ' --file /home/activus/data/' + database + '/ss_customer.csv --type csv --remove-attribute ":TYPE" ' \
       '--from-collection-prefix Store --to-collection-prefix Customer --separator "," ' \
       '--translate ":START_ID(Store)=_from" --translate ":END_ID(Customer)=_to" ' \
       '--create-collection true --create-collection-type edge --overwrite true ' \
       '--collection "SS_Customer"'
os.system(cmd)

# import SS_Item
cmd = 'arangoimp --server.username root --server.password activus01 --server.database ' + database + \
       ' --file /home/activus/data/' + database + '/ss_item.csv --type csv --remove-attribute ":TYPE" ' \
       '--from-collection-prefix Store --to-collection-prefix Item --separator "," ' \
       '--translate ":START_ID(Store)=_from" --translate ":END_ID(Item)=_to" ' \
       '--create-collection true --create-collection-type edge --overwrite true ' \
       '--collection "SS_Item"'
os.system(cmd)

# import SS_Customer_Item
cmd = 'arangoimp --server.username root --server.password activus01 --server.database ' + database + \
       ' --file /home/activus/data/' + database + '/ss_customer_item.csv --type csv --remove-attribute ":TYPE" ' \
       '--from-collection-prefix Customer --to-collection-prefix Item --separator "," ' \
       '--translate ":START_ID(Customer)=_from" --translate ":END_ID(Item)=_to" ' \
       '--create-collection true --create-collection-type edge --overwrite true ' \
       '--collection "SS_Customer_Item"'
os.system(cmd)

# import SS_Promotion
cmd = 'arangoimp --server.username root --server.password activus01 --server.database ' + database + \
       ' --file /home/activus/data/' + database + '/ss_promotion.csv --type csv --remove-attribute ":TYPE" ' \
       '--from-collection-prefix Store --to-collection-prefix Promotion --separator "," ' \
       '--translate ":START_ID(Store)=_from" --translate ":END_ID(Promotion)=_to" ' \
       '--create-collection true --create-collection-type edge --overwrite true ' \
       '--collection "SS_Promotion"'
os.system(cmd)


# import WR_Customer
cmd = 'arangoimp --server.username root --server.password activus01 --server.database ' + database + \
       ' --file /home/activus/data/' + database + '/wr_customer.csv --type csv --remove-attribute ":TYPE" ' \
       '--from-collection-prefix Web_Page --to-collection-prefix Customer --separator "," ' \
       '--translate ":START_ID(Web_Page)=_from" --translate ":END_ID(Customer)=_to" ' \
       '--create-collection true --create-collection-type edge --overwrite true ' \
       '--collection "WR_Customer"'
os.system(cmd)

# import WR_Item
cmd = 'arangoimp --server.username root --server.password activus01 --server.database ' + database + \
       ' --file /home/activus/data/' + database + '/wr_item.csv --type csv --remove-attribute ":TYPE" ' \
       '--from-collection-prefix Web_Page --to-collection-prefix Item --separator "," ' \
       '--translate ":START_ID(Web_Page)=_from" --translate ":END_ID(Item)=_to" ' \
       '--create-collection true --create-collection-type edge --overwrite true ' \
       '--collection "WR_Item"'
os.system(cmd)

# import WR_Customer_Item
cmd = 'arangoimp --server.username root --server.password activus01 --server.database ' + database + \
       ' --file /home/activus/data/' + database + '/wr_customer_item.csv --type csv --remove-attribute ":TYPE" ' \
       '--from-collection-prefix Customer --to-collection-prefix Item --separator "," ' \
       '--translate ":START_ID(Customer)=_from" --translate ":END_ID(Item)=_to" ' \
       '--create-collection true --create-collection-type edge --overwrite true ' \
       '--collection "WR_Customer_Item"'
os.system(cmd)

# import WS_Customer
cmd = 'arangoimp --server.username root --server.password activus01 --server.database ' + database + \
       ' --file /home/activus/data/' + database + '/ws_customer.csv --type csv --remove-attribute ":TYPE" ' \
       '--from-collection-prefix Web_Page --to-collection-prefix Customer --separator "," ' \
       '--translate ":START_ID(Web_Page)=_from" --translate ":END_ID(Customer)=_to" ' \
       '--create-collection true --create-collection-type edge --overwrite true ' \
       '--collection "WS_Customer"'
os.system(cmd)

# import WS_Item
cmd = 'arangoimp --server.username root --server.password activus01 --server.database ' + database + \
       ' --file /home/activus/data/' + database + '/ws_item.csv --type csv --remove-attribute ":TYPE" ' \
       '--from-collection-prefix Web_Page --to-collection-prefix Item --separator "," ' \
       '--translate ":START_ID(Web_Page)=_from" --translate ":END_ID(Item)=_to" ' \
       '--create-collection true --create-collection-type edge --overwrite true ' \
       '--collection "WS_Item"'
os.system(cmd)

# import WS_Customer_Item
cmd = 'arangoimp --server.username root --server.password activus01 --server.database ' + database + \
       ' --file /home/activus/data/' + database + '/ws_customer_item.csv --type csv --remove-attribute ":TYPE" ' \
       '--from-collection-prefix Customer --to-collection-prefix Item --separator "," ' \
       '--translate ":START_ID(Customer)=_from" --translate ":END_ID(Item)=_to" ' \
       '--create-collection true --create-collection-type edge --overwrite true ' \
       '--collection "WS_Customer_Item"'
os.system(cmd)

# import WS_Promotion
cmd = 'arangoimp --server.username root --server.password activus01 --server.database ' + database + \
       ' --file /home/activus/data/' + database + '/ws_promotion.csv --type csv --remove-attribute ":TYPE" ' \
       '--from-collection-prefix Web_Page --to-collection-prefix Promotion --separator "," ' \
       '--translate ":START_ID(Web_Page)=_from" --translate ":END_ID(Promotion)=_to" ' \
       '--create-collection true --create-collection-type edge --overwrite true ' \
       '--collection "WS_Promotion"'
os.system(cmd)


# import ws_web_site_web_page
cmd = 'arangoimp --server.username root --server.password activus01 --server.database ' + database + \
       ' --file /home/activus/data/' + database + '/ws_web_site_web_page.csv --type csv --remove-attribute ":TYPE" ' \
       '--from-collection-prefix Web_Page --to-collection-prefix Web_Site --separator "," ' \
       '--translate ":START_ID(Web_Page)=_from" --translate ":END_ID(Web_Site)=_to" ' \
       '--create-collection true --create-collection-type edge --overwrite true ' \
       '--collection "WS_Web_Site_Web_Page"'
os.system(cmd)

time_end = time.time()
print('time cost', time_end - time_start, 's') #time of the import
