Experimental assessments in the article "Modelling, storing and querying temporal graphs" (Submission to International Journal of Information Management 2021)


/!\ The personal information of this gitlab account is temporarily anonymous during the process review of the article in ISF 2021. It is not possible to contact this account until the end of the process review. /!\


***Abstract of the paper***

This paper proposes a conceptual model relying on a graph to represent the complete temporal evolution of a business application while limiting data redundancy. This new modelling solution captures the temporal evolution at the level of: (i) the structure of relationships between entities, (ii) the attribute set of entities/relationships and (iii) the attributes’ value of entities/relationships. Keeping temporal evolution inevitably increases data volume. We carry out a systematic study on storing and querying temporal graph data according to our proposed modelling solution. Specifically, we implement our model in three graph data stores, namely Neo4j, OrientDB and ArangoDB. We propose 30 benchmark queries and execute them in each data store to study their querying efficiency. The contribution of our experimental assessments is to provide guidelines to design a temporal graph from conceptual modelling to its implementation into a graph data store. Moreover, we provide some recommendations according to user needs.

***Goal of the experimental assessments***

Our experimental assessments aim at (i) illustrating that our modelling solution is easily convertible and usable into a graph data store, (ii) comparing the querying performance of the three graph data stores: Neo4j, OrientDB, ArangoDB. 

This Gitlab project presents the material and methods we use to run these experiments.


***Technical environment***

The hardware configuration of our technical environment is as follows: PowerEdge R630, 16 CPUs x Intel(R) Xeon(R) CPU E5-2630 v3 @ 2.40Ghz, 63.91 GB. Three virtual machines are installedon this hardware. Each virtual machine has 6GB in terms of RAM and 100GB in terms of disk size. 

On each of the three virtual machines, we installed respectively the community editions of (i) Neo4j (version 4.1.3), (ii) OrientDB (version 3.0.4) and (iii) ArangoDB (version 3.4.10) datastores.

We used python programming language in the Pycharm IDE to: 
- transform our datasets
- import our datasets in Neo4j and ArangoDB 
- run queries and collect their execution times.

We also used SQL lite in one step of the datasets transformation. 

***Datasets***

To not generate data by ourselves, we used the TPC-DS benchmark. This benchmark provides a typical dataset about sales and sales returns process to support a decision-maker. The dataset contains dimension tables and fact tables about vital business information, such as store, item and sales data. Moreover, it integrates a time dimension to capture the changes that occured in the business application over time.

1) The [TPC-DS benchmark](http://www.tpc.org/tpcds/) allows to generate datasets according to parameterized data volumes. We generated 4 datasets according to 4 scale factors to obtain 4 different data volumes:
- get the generator toolkit : download it from the [TPC-DS website](http://tpc.org/tpc_documents_current_versions/download_programs/tools-download-request5.asp?bm_type=TPC-DS&bm_vers=2.13.0&mode=CURRENT-ONLY) or download it from the directory [TPC-DS](https://gitlab.com/2573869/tgmsq/-/tree/3aff15b2eba8751869fdb4d878d24e5effe5ce28/TPC-DS) of this project.
- follow the guidelines to use the the generator toolkit [here](https://datacadamia.com/data/type/relation/benchmark/tpcds/load#data_generation). We used four volume parameters to run the generator: 1 to generate 1GB, 2 to generate 2GB, 4 to generate 4GB and 8 to generate 8GB. 


2) We transformed the TPC-DS dataset into our temporal graph representation using python programs (.py) in this project. 

For each generated (for each data volume), we followed the transformation process presented at the file [data_transformation_process.png](https://gitlab.com/2573869/tgmsq/-/blob/3aff15b2eba8751869fdb4d878d24e5effe5ce28/data_transformation.pdf). This file specifies which python program to run at each step. The transformed datasets have the following volumes: DS1 = 0.9GB, DS2 = 1.7GB, DS3 = 3.7GB, DS4 = 7.5GB. 

N.B: After transformation, we made an additional modification on the file has_household_demographics.csv for each dataset due to an error we made. We used the python  program [modify_hhd.py](https://gitlab.com/2573869/tgmsq/-/blob/3aff15b2eba8751869fdb4d878d24e5effe5ce28/modify_hhd.py).

You can download our transformed datasets [here](https://filesender.renater.fr/?s=download&token=90672d79-deec-4883-95e2-3afe7ad0eae0). In this file, DS1 is called data1gb, DS2 is called data2gb, DS3 is called data4gb and DS4 is called data8gb. 


3) We imported each of the 4 transformed datasets in each data store following the importation process presented at the file [import_process.png](https://gitlab.com/2573869/tgmsq/-/blob/master/import_process.PNG). 

***Benchmark queries***

We translated the benchmark queries in the query language for each data store. Download the file [Queries.zip](https://gitlab.com/2573869/tgmsq/-/blob/master/Queries.zip) which includes the queries written in Cypher (Neo4j query language), in AQL (ArangoDB query language) and in SQL (OrientDB query language).
 

***Querying performance*** 
 
To evaluate the querying performance of each data store as the data volume increases, we record the execution time, which is the elapsed
time (in seconds), for processing each benchmark query on the 4 datasets. We run each benchmark query ten times and take the
mean time of all runs as final execution time. It is worth noticing that we do not use any customized optimization techniques but rely on default tuning to avoid any bias in the querying performance. 

We present our querying process for each data volume at the file [querying_process.png](https://gitlab.com/2573869/tgmsq/-/blob/master/querying_process.PNG). Download the input and output files of the querying process [here](https://gitlab.com/2573869/tgmsq/-/blob/master/Queries.zip). 


