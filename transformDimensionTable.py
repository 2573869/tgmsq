#run on my local machine

# this module allows:

##### to transform the .dat files to .csv files: reason, promotion, promotion_item, customer,  has_customer_address, has_customer_demographics, customer_address, household_demographics, has_income_band, ship_mode, warehouse

##### to call evolution.py to transform the .dat files to .csv files and add evolution : call_center, catalog_page, item, promotion, store, web_page and web_site

import os
import pandas as pd
import time

# You need to change the path_0 and path_1 to read the files and save the files with respect to the volume you choose
# where we find the data .dat
path_0 = '/Users/arslan/Desktop/data16GB/'
# where we create the csv
path_1 = '/Users/arslan/Desktop/data16GB/CSV/'
time_start = time.time()

# execute evolution.py file which will transfer and add evolution data of
# call_center, catalog_page, item, promotion, store, web_page and web_site
os.system("python evolution.py")

# load date
dir_path_date = os.path.join(path_0, "date_dim.dat")
dfDate = pd.read_table(dir_path_date, sep="|", header=None)
dfDate = dfDate.iloc[:, [0, 2]]
dfDate.columns = ["date_sk", "date"]

# transfer reason
dir_path = os.path.join(path_0, "reason.dat")
new_path = os.path.join(path_1, "reason.csv")
df = pd.read_table(dir_path, sep="|", header=None)

df.columns = ["reason_sk:ID(Reason)", "reason_id", "reason_desc", ":LABEL"]
df[':LABEL'] = 'Reason'
df.to_csv(new_path, index=False, sep=",") #create the csv file of Reason

# transfer promotion
dir_path = os.path.join(path_0, "promotion.dat")
new_path = os.path.join(path_1, "promotion.csv")
df = pd.read_table(dir_path, sep="|", header=None)

df = df.iloc[:, [0, 1, 2, 3, 4, 5, 7, 8, 16, 18]]
df.columns = ["promo_sk:ID(Promotion)", "promo_id", "date_sk", "end_date_sk", "item_sk", "cost", "promo_name",
              "channel_dmail", "channel_details", "discount_active"]

df = df.fillna(0)
df['item_sk'] = df['item_sk'].astype(int)

df = pd.merge(df, dfDate, on="date_sk")
df = df.drop(columns=["date_sk"])
df.rename(columns={'date': 'start_valid_time'}, inplace=True)
df.rename(columns={'end_date_sk': 'date_sk'}, inplace=True)
df = pd.merge(df, dfDate, on="date_sk")
df = df.drop(columns=["date_sk"])
df.rename(columns={'date': 'end_valid_time'}, inplace=True)

df[':LABEL'] = 'Promotion'
df.to_csv(new_path, index=False, sep=",")

# create promotion_item
df_relation = pd.DataFrame(df, columns=['promo_sk:ID(Promotion)', 'start_valid_time', 'end_valid_time', 'item_sk'])
df_relation.columns = [":START_ID(Promotion)", "start_valid_time", "end_valid_time", "item_sk"]
df_relation[':TYPE'] = 'Promotion_Item'
df_relation.insert(1, 'promo_sk', df["promo_sk:ID(Promotion)"])
df_relation.insert(4, ':END_ID(Item)', df["item_sk"])

new_path = os.path.join(path_1, "promotion_item.csv")
df_relation.to_csv(new_path, index=False, sep=",")

# transfer customer
dir_path = os.path.join(path_0, "customer.dat")
new_path = os.path.join(path_1, "customer.csv")
df = pd.read_table(dir_path, sep="|", header=None, encoding='latin-1')

df.columns = ["customer_sk:ID(Customer)", "customer_id", "current_cdemo_sk", "current_hdemo_sk", "current_addr_sk",
              "date_sk", "first_sales_date_sk", "salutation", "first_name", "last_name", "preferred_cust_flag",
              "birth_day", "birth_month", "birth_year", "birth_country", "login", "email_address", "last_review_date",
              ":LABEL"]

df = df.fillna(0)
df['customer_sk:ID(Customer)'] = df['customer_sk:ID(Customer)'].astype(int)
df['current_cdemo_sk'] = df['current_cdemo_sk'].astype(int)
df['current_hdemo_sk'] = df['current_hdemo_sk'].astype(int)
df['current_addr_sk'] = df['current_addr_sk'].astype(int)

df = pd.merge(df, dfDate, on="date_sk")
df = df.drop(columns=["date_sk"])
df.rename(columns={'date': 'first_shipto_date_sk'}, inplace=True)
df.rename(columns={'first_sales_date_sk': 'date_sk'}, inplace=True)
df = pd.merge(df, dfDate, on="date_sk")
df = df.drop(columns=["date_sk"])
df.rename(columns={'date': 'first_sales_date_sk'}, inplace=True)
df.rename(columns={'last_review_date': 'date_sk'}, inplace=True)
df = pd.merge(df, dfDate, on="date_sk")
df = df.drop(columns=["date_sk"])
df.rename(columns={'date': 'last_review_date'}, inplace=True)

df[':LABEL'] = 'Customer'
df = df.drop(columns=["login"])
df.to_csv(new_path, index=False, sep=",")

# create has_customer_address
df_relation = pd.DataFrame(df, columns=['customer_sk:ID(Customer)', 'current_addr_sk'])
df_relation.columns = [":START_ID(Customer)", ":END_ID(Customer_Address)"]
df_relation[':TYPE'] = 'Has_Customer_Address'

new_path = os.path.join(path_1, "has_customer_address.csv")
df_relation.to_csv(new_path, index=False, sep=",")

# create has_customer_demographics
#extract columns customer_sk and current_cdemo_sk from table CUSTOMER
df_relation = pd.DataFrame(df, columns=['customer_sk:ID(Customer)', 'current_cdemo_sk'])

#rename the two columns customer_sk and current_cdemo_sk
df_relation.columns = [":START_ID(Customer)", ":END_ID(Customer_Demographics)"]


df_relation[':TYPE'] = 'Has_Customer_Demographics'

new_path = os.path.join(path_1, "has_customer_demographics.csv")
df_relation.to_csv(new_path, index=False, sep=",") #create the csv file of has_customer_demographics


# transfer customer_address
dir_path = os.path.join(path_0, "customer_address.dat")
new_path = os.path.join(path_1, "customer_address.csv")
df = pd.read_table(dir_path, sep="|", header=None)

df.columns = ["address_sk:ID(Customer_Address)", "address_id", "street_number", "street_name", "street_type",
              "suite_number", "city", "county", "state", "zip", "country",
              "gmt_offset", "location_type", ":LABEL"]

df[':LABEL'] = 'Customer_Address'
df.to_csv(new_path, index=False, sep=",")

# transfer customer_demographics
dir_path = os.path.join(path_0, "customer_demographics.dat")
new_path = os.path.join(path_1, "customer_demographics.csv")
df = pd.read_table(dir_path, sep="|", header=None)

df.columns = ["cd_demo_sk:ID(Customer_Demographics)", "gender", "martial_status", "education_status",
              "purchase_estimate", "credit_rating", "dep_count", "employed_count", "college_count", ":LABEL"]

df[':LABEL'] = 'Customer_Demographics'
df.to_csv(new_path, index=False, sep=",")

# transfer household_demographics
dir_path = os.path.join(path_0, "household_demographics.dat")
new_path = os.path.join(path_1, "household_demographics.csv")
df = pd.read_table(dir_path, sep="|", header=None)

df.columns = ["hd_demo_sk:ID(Household_Demographics)", "income_band_sk", "buy_potential",
              "dep_count", "vehicle_count", ":LABEL"]

df = df.fillna(0)
df['income_band_sk'] = df['income_band_sk'].astype(int)

df[':LABEL'] = 'Household_Demographics'
df.to_csv(new_path, index=False, sep=",")

# create has_income_band
df_relation = pd.DataFrame(df, columns=['hd_demo_sk:ID(Household_Demographics)', 'income_band_sk'])
df_relation.columns = [":START_ID(Household_Demographics)", ":END_ID(Income_Band)"]
df_relation[':TYPE'] = 'Has_Income_Band'

new_path = os.path.join(path_1, "has_income_band.csv")
df_relation.to_csv(new_path, index=False, sep=",")

# transfer income_band
dir_path = os.path.join(path_0, "income_band.dat")
new_path = os.path.join(path_1, "income_band.csv")
df = pd.read_table(dir_path, sep="|", header=None)

df.columns = ["income_band_sk:ID(Income_Band)", "lower_band", "upper_band", ":LABEL"]

df[':LABEL'] = 'Income_Band'
df.to_csv(new_path, index=False, sep=",")

# transfer ship_mode
dir_path = os.path.join(path_0, "ship_mode.dat")
new_path = os.path.join(path_1, "ship_mode.csv")
df = pd.read_table(dir_path, sep="|", header=None)

df.columns = ["ship_mode_sk:ID(Ship_Mode)", "ship_mode_id", "type", "code",
              "carrier", "contract", ":LABEL"]

df[':LABEL'] = 'Ship_Mode'
df.to_csv(new_path, index=False, sep=",")

# transfer warehouse
dir_path = os.path.join(path_0, "warehouse.dat")
new_path = os.path.join(path_1, "warehouse.csv")
df = pd.read_table(dir_path, sep="|", header=None)

df.columns = ["warehouse_sk:ID(Warehouse)", "warehouse_id", "warehouse_name", "warehouse_sq_ft",
              "street_number", "street_name", "street_type", "suite_number", "city", "county",
              "state", "zip", "country", "gmt_offset", ":LABEL"]

df[':LABEL'] = 'Warehouse'
df.to_csv(new_path, index=False, sep=",")


time_end = time.time()
print('time cost', time_end - time_start, 's')
